\chapter{Introduction}

\label{sec:context}
%Describe here the context within which the thesis must be positioned.

\fbox{\parbox{\textwidth}{\uline{Disclaimer:}
    The research work described in this PhD Dissertation is joint work with Dr. Zhengbing Bian, Dr.Fabian Chudak, Dr.William Macready, Dr.Aidan Roy from D-Wave System Inc. and with my advisor Prof.Roberto Sebastiani from Università di Trento. Most of the scientific content presented here can be found in the papers:
\begin{itemize}
    \item \bibentry{bian_frocos17}.
    \item \bibentry{svpaper}.
\end{itemize}
}}

%\footnotetext{\url{https://arxiv.org/abs/1811.02524}}
\bigskip

The development of quantum computing theory has been one of the most intense
area of research in complexity theoretical computer science in recent years. Many ways have
been devised to exploit quantum weirdness to obtain computational speedups, but
the development of quantum computing hardware has been stuck by the high
susceptibility to noise. While noise tolerant quantum circuits are slowly
improving over time, the currently available hardware shows a behavior that is
typical of quantum systems but provides limited computational flexibility.
Quantum annealers accept some level of noise and decoherence to allow for large
quantum systems.

\section{The Problem} \label{sec:problem} 

Quantum annealers are able to solve a specific subset of a single optimization
problem. Whereas this problem is NP-hard, and thus it is theoretically possible
to convert any NP problem instance into such problem, in practice the process
of encoding is difficult due to several limitations.

The thesis will focus on SAT problems, as alternative tools able to tackle
worst-case instances would be critically useful. Whereas the hardware is able to
solve several NP-hard problems it is not particularly suited to solve SAT
problems. To demonstrate the capabilities of quantum annealing, SAT problems
that are considerably hard for state-of-the-art SAT solvers are preferred.
Encoding such a problem into a quantum annealing problem would provide a
convincing evaluation for the approach.

\section{The Solution}
\label{sec:solution}

In this thesis, a method is proposed to encode SAT and MaxSAT problems into
quantum annealing problems. First, a method to generate optimal encodings for
Boolean functions is outlined. This method uses SMT solvers and requires
knowing the models and counter-models of the Boolean function, and thus it is
not applicable to solve large SAT problems. SMT solving is instead used to
build a library of efficiently encoded gates.

The second part of the solution consists in a multi-step process where the input
Boolean formula is broken into multiple components from the pre-encoded library
and these elements are re-composed into a final quantum annealing problem. This
part uses a heuristic approach to shape the input formula into an encoding
that respects the hardware constraints.

This approach provides an effective and efficient method for SAT problem
encoding. It is thought for SAT solving of generic Boolean circuits, while the
state of the art is focused either on optimization problem or in specific
constraint satisfaction problems.


\section{Structure of the Thesis} \label{sec:structure} 

The first half of the thesis, containing Chapter \ref{chp:motivations} to
\ref{chp:related-work}, will provide the context and background for the thesis.
The second chapter will outline the motivations behind this area of research
and the goals of this thesis, why quantum computing can be useful and what are
the current limits. The third chapter will provide a comprehensive background
on quantum computing, quantum annealing and other aspects of quantum
computation, and then it will introduce various logic problems such as SAT,
MaxSAT, SMT and OMT. The fourth chapter will survey related work on the same problem and highlight differences and limitations.

The second half of the thesis, from Chapter \ref{chp:foundations} to
\ref{chp:exper}, will show the novel contributions. The fifth chapter will lay
the theoretical foundations for the rest of the work, stating concepts such as
penalty functions, variable placement, and embedding. The sixth chapter will
explain how to use SMT and OMT solvers to find the optimal encoding of a
Boolean function and possibly a placement. The seventh chapter will provide a
framework for encoding large SAT problems using functions pre-encoded with the
techniques explained in the previous chapter. Each of the steps will be
explained in details. The eighth chapter will describe an implementation of the
work, describing details and usage of several Python libraries. The basic file
formats, encoding tasks and command line interfaces will be described. Finally,
The ninth chapter will provide an experimental evaluation of the techniques on
SAT and MaxSAT problems, with details on the choice of the benchmark problem.
