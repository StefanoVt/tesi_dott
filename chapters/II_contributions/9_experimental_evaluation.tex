\chapter{Experimental evaluation\label{chp:exper}}

Using the software described in Chapter \ref{chp:implementation}, the chapter
provides a preliminary empirical validation of the proposed methods for
\sattoqubo{} encoding and SAT solving by evaluating the performance of D-Wave's
2000Q quantum annealer in solving certain hard SAT problems
(Section~\ref{sec:sat-exper}); a similar evaluation is performed on MaxSAT problems as
well (Section~\ref{sec:maxsat-exper}).

Currently the most time-onerous step in the on-the-fly part of the process
outlined in Chapter \ref{chp:large} is the placement and routing step.
Encoding the problems in the following evaluation requires approximately 20
minutes on an Intel i7-5600U CPU. However software heuristics are heavily
tunable in order to trade off efficiency and effectiveness of the
place-and-route process. As long as the encoding process does not become
untractable, we can focus on the time taken by the quantum annealer to reach
the final solution state. Furthermore, in certain contexts (e.g. fault diagnosis \cite{bian2016mapping}), hardware embeddings are reusable and therefore can be thought as a one-time cost.

This evaluation has a number of requirements. First, we require instances that
can be entirely encoded in the qubits of a currently available quantum
annealer, i.e. a Chimera graph of around 2000 qubits (although algorithms for
solving larger CSP with QA have been proposed
\cite{bian2014discrete,bian2016mapping}). Furthermore, SAT solvers are already
quite effective on average case problems, thus we need concrete problems that
can get state-of-the-art solvers stuck. Another important consideration in
solving [Max]SAT instances is that the QA hardware cannot reason on the input
or produce proofs. Thus unsatisfiable SAT instances are not suitable for
evaluation.

QA hardware behaves more like an SLS solver than a CDCL-based one: for this
reason we solved the same problems with the state-of-the-art \textsc{UBCSAT}
SLS SAT solver using the best performing algorithm, namely SAPS
\cite{tompkins_ubcsat:_2004}. \textsc{UBCSAT} was run on a computer using a
8-core Intel\textsuperscript{\textregistered}
Xeon\textsuperscript{\textregistered} E5-2407 CPU, at 2.20GHz.

The results reported in this section are not intended as a performance
comparison between D-Wave's 2000Q system and \textsc{UBCSAT}, or any other
classic computing tool. There are issues in comparing specialized vs.
off-the-shelf hardware and different timing mechanisms and timing
granularities. Rather than that the aim is to provide an experimental
assessment of the potential use of quantum annealing for [Max]SAT solving.


Experimental data,  problem files, translation files, 
demonstration source
code and supplementary material can be accessed from a publicly available website\footnote{\ \url{https://bitbucket.org/aqcsat/aqcsat}.}. 
A D-Wave 2000Q machine is publicly accessible through
D-Wave's Leap
cloud service~\footnote{\ \url{https://cloud.dwavesys.com/leap/}.}.


% \begin{IGNOREINLONG}
% Due to the limitations in size and connectivity of current QA systems,
% we require [Max]SAT problems that become difficult with few variables. To
% this end we modified the tool \sgen{}~\cite{spence_sgen1:_2010}, which
% has been used to generate the smallest unsolvable problems in recent
% SAT competitions. In particular, we modified \sgen{} to use 2-in-4-SAT
% constraints instead of at-most/at-least 1-in-5-SAT constraints, as
% 2-in-4-SAT is particularly suitable to encoding with Ising
% models (see~\cite{bian17_frocos17extended} for details).
%  We generated
% 100 problem instances for various problem sizes up to 80 variables,
% the largest embeddable with current hardware. At 260
% variables, these problems become unsolvable within 1000 seconds with
% state-of-the-art SAT solvers on standard machines 
%~\cite{bian17_frocos17extended}.  
% \end{IGNOREINLONG}


\section{SAT Experiments}
\label{sec:sat-exper}

\begin{figure}
  \includegraphics[width=\textwidth]{figures/old/asymp_sat_scaled.pdf}
\caption[Median times vs. problem size for the best-performing SLS algorithm, on two variants of the \sgen{} problem on \textsc{UBCSAT} (SAPS).]{Median times vs. problem size for the best-performing SLS algorithm, on two variants of the \sgen{} problem on \textsc{UBCSAT} (SAPS). Timeout
    is at 1000 seconds.
The figure report times on a 8-core
Intel\textsuperscript{\textregistered}
Xeon\textsuperscript{\textregistered} E5-2407 CPU, at 2.20GHz. 
\label{fig:asymp_sat}}
 \end{figure}

%\paragraph{Choosing the benchmarks}
\subsection{Choosing the benchmark problems}
\label{sec:choosing-benchmarks}
Due to previously mentioned limitations in size and connectivity of current QA systems,
we require SAT problems which have a small number of variables but are hard for standard SAT solvers in general. 

To this end the chosen benchmarks are created with the tool \sgen{}
\cite{spence_sgen1:_2010} with some modifications. \sgen{} is the current state
of the art for generating the small unsolvable problems in recent SAT
competitions. Furthermore, the problems have a structure that is suited for problem
embedding, as they are composed of a single type of  constraint that can be embedded very efficiently, and problems with few hundreds of  variables are considerably hard. The \sgen{} family of generators received many improvements over the years, but satisfiable instances are generated in the same way \cite{gelder_zeroone_2010,spence_weakening_2015}. \sgen{} creates problems by setting 
cardinality constraints over different partitions of the set of variables. The tool requires as an input the desired problem size for the output (and a random number generator). Given this, the
generator operates as follows:

\begin{enumerate}
\item A satisfying assignment is decided at random.
\item The tool partitions the variable set into sets of 5 elements in such a way that  each subset contains exactly
one true variable for the desired solution. 
\item For each subset we guarantee that at most
one variable is true (10 2-CNF clauses).
\item The partition is shuffled into a new one. The tool ensures that each new subset contain
exactly one true variable, and minimizes the similarity with the previous
partition. 
\item For each partition subset we ensure that at least one variable is true (a
single CNF clause).

\item The previous two steps are repeated one more time, to further restrict
the solution space.

\end{enumerate}

In Figure~\ref{fig:asymp_sat}, the red dots represent median resolution times 
for \textsc{UBCSAT} SAPS on random \sgen{} problems. Notice that with $>300$ variables the solver
reaches the timeout of 1000 seconds for all problems. 

In our validation experiments, we modify the tool to better suit the annealer
hardware. We use exactly-2-in-4 constraints on partitions with sets of size 4
instead of size 5 partitions, with exactly two true variables per subset. This
 constraint has a very efficient embedding and furthermore the modified problems are slightly
harder with the same number of variables (see the blue dots on Figure \ref{fig:asymp_sat}, where \textsc{UBCSAT} SAPS
reaches timeout with $>270$ variables).

\subsection{Experiments and Results}
% 



We generated several problem instances with multiple problem sizes. 100 different problems are generated per size, with size ranging from 32 variables to 80, the biggest size
on which embedding has been successfully performed. Furthermore problems have been
generated in the original version (with at-least-one-in-five and
at-most-one-in-five) and a two-in-four version (using exactly-two-in-four
constraints). 
These SAT instances are  encoded and embedded using the divide-and-conquer method.

For the experiment, a fixed number of samples/instance (5, 10, 20) is drawn from the quantum annealer.
Annealing was executed at a rate of
\SI{10}{\micro\second} per sample, for a total of
\SI{50}{\micro\second}, \resp{\SI{100}{\micro\second} and
\SI{200}{\micro\second}}    of anneal time per instance respectively. Total time used by
the D-Wave processor includes programming and readout; this amounts to
about \SI{150}{\micro\second} per sample, plus a constant
\SI{10}{\milli\second} of overhead.
Table~\ref{tab:satresults}\subref{tab:dw_sat_solved} shows the results using the D-Wave 2000Q annealer.
The quantum annealer solves almost all problems with 5 samples (i.e. within
\SI{50}{\micro\second} of total anneal time), and all of them are solved with 20
samples (i.e. within \SI{200}{\micro\second} of total anneal time).
In order to contextualize the results, the
same problems are solved with the \textsc{UBCSAT} SLS SAT solver, using SAPS
\cite{tompkins_ubcsat:_2004}.  These computations were
performed using an 8-core Intel\textsuperscript{\textregistered}
Xeon\textsuperscript{\textregistered} E5-2407 CPU, at 2.20GHz. 
Table~\ref{tab:satresults}\subref{tab:ubcsat-saps-avgtimes}
shows that the problems are nontrivial despite the small number of
variables, and the run-times increase significantly with the size of
the problem. (See also Figure~\ref{fig:asymp_sat}.) 


\begin{table}[htb]
  \small
  \begin{center}
  \subtable[]{\label{tab:dw_sat_solved}
  \begin{tabular}{||l||r|r|r||r||}
  \hline
  \multicolumn{5}{|c|}{\textbf{D-Wave 2000Q}}\\\hline
  \textbf{Problem size}&\textbf{\begin{tabular}{@{}c@{}}\# solved\\5 samples\end{tabular}}& \textbf{\begin{tabular}{@{}c@{}}\# solved\\10 samples\end{tabular}} & \textbf{\begin{tabular}{@{}c@{}}\# solved\\20 samples\end{tabular}} &\textbf{\begin{tabular}{@{}c@{}}\% optimal\\samples\end{tabular}}\\\hline
  \textbf{32 vars}&100&100&100&97.4\\\hline
  \textbf{36 vars}&100&100&100&96.4\\\hline
  \textbf{40 vars}&100&100&100&94.8\\\hline
  \textbf{44 vars}&100&100&100&93.8\\\hline
  \textbf{48 vars}&100&100&100&91.4\\\hline
  \textbf{52 vars}&100&100&100&93.4\\\hline
  \textbf{56 vars}&100&100&100&91.4\\\hline
  \textbf{60 vars}&100&100&100&88.2\\\hline
  \textbf{64 vars}&100&100&100&84.6\\\hline
  \textbf{68 vars}&100&100&100&84.4\\\hline
  \textbf{72 vars}&98 &100&100&84.6\\\hline
  \textbf{76 vars}&99 &99&100&86.6\\\hline
  \textbf{80 vars}&100&100&100&86.0\\\hline
  \end{tabular}}
  %\begin{tabular}{|l|r|r|}
  %\hline
  %\multicolumn{3}{|c|}{\textbf{D-Wave 2000Q}}\\\hline
  %\textbf{Problem size}&\textbf{\#  solved}&\textbf{\begin{tabular}{@{}c@{}}\% optimal\\samples\end{tabular}}\\\hline
  %\textbf{32 vars}&100&97.4\\\hline
  %\textbf{36 vars}&100&96.4\\\hline
  %\textbf{40 vars}&100&94.8\\\hline
  %\textbf{44 vars}&100&93.8\\\hline
  %\textbf{48 vars}&100&91.4\\\hline
  %\textbf{52 vars}&100&93.4\\\hline
  %\textbf{56 vars}&100&91.4\\\hline
  %\textbf{60 vars}&100&88.2\\\hline
  %\textbf{64 vars}&100&84.6\\\hline
  %\textbf{68 vars}&100&84.4\\\hline
  %\textbf{72 vars}&98 &84.6\\\hline
  %\textbf{76 vars}&99 &86.6\\\hline
  %\textbf{80 vars}&100&86.0\\\hline
  %\end{tabular}}
  %\qquad
  \subtable[]{\label{tab:ubcsat-saps-avgtimes}
  \begin{tabular}{|l|r|}
  \hline
  \multicolumn{2}{|c|}{\textbf{\textsc{UBCSAT} (SAPS)}}\\\hline
  \textbf{Problem size}&\textbf{Avg time (ms)}\\\hline
  \textbf{32 vars}&0.1502\\\hline
  \textbf{36 vars}&0.2157\\\hline
  \textbf{40 vars}&0.3555\\\hline
  \textbf{44 vars}&0.5399\\\hline
  \textbf{48 vars}&0.8183\\\hline
  \textbf{52 vars}&1.1916\\\hline
  \textbf{56 vars}&1.4788\\\hline
  \textbf{60 vars}&2.2542\\\hline
  \textbf{64 vars}&3.1066\\\hline
  \textbf{68 vars}&4.8058\\\hline
  \textbf{72 vars}&6.2484\\\hline
  \textbf{76 vars}&8.2986\\\hline
  \textbf{80 vars}&12.4141\\\hline
  \end{tabular}}
  \caption{\label{tab:satresults}%
  (a) Number of \sattoqubo{} problem instances (out of 100) solved by the QA hardware
  using 5 samples \resp{10 and 20} and average fraction of samples from the QA hardware
  that are optimal solutions.  \newline
  (b) Run-times in
  \SI{}{\milli\second} for SAT instances solved by \textsc{UBCSAT} using SAPS,
  averaged over 100 instances of each problem size.
  }
  \end{center}
  \end{table}

\section{MaxSAT experiments}

Exact penalty functions (Section \ref{sec:foundations}) allow for the encoding of weighted MaxSAT problems, {with some restrictions}. 
To demonstrate the performance of the QA hardware in this regime, 
we generated weighted MaxSAT instances that have many distinct optimal solutions.
%One of the strengths of D-Wave's QA hardware is its ability to rapidly sample the near-optimal solutions: current machines typically anneal at a rate of \SI{10}{\micro\second} or \SI{20}{\micro\second} per sample and are designed to take thousands of samples during each programming cycle. As a result it is likely that the first practical benefits of QA will come from applications which require many solutions rather than a single optimum. 


\label{sec:maxsat-exper}

\subsection{Choosing the benchmarks}
 The weighted MaxSAT problems were generated from the previous 
2-in-4-SAT instances by removing part of the constraint, turning into  unsatisfiable instances and then adding constraints on single variables with smaller weight. More precisely:

\begin{enumerate}
\item Start with the 2-in-4-SAT instances of the previous section.
 
\item Remove one of the partitions of the variable set, and change one 2-in-4
constraint to 1-in-4. This makes the SAT problem unsatisfiable: for an $n$
variable problem, the first partition demands exactly $n/2$ true variables,
while the second demands exactly $n/2-1$.

\item Each constraint is assigned a soft weight of 3 and for each variable
single literal constraint with random polarity is created and assigned weight
1.

\item Multiple MaxSAT instances of this form are generated  until an instance has the optimal solution with exactly one violated clause of weight 3 and at least $n/3$ violated clauses of weight 1, and with at least $200$ distinct optimal solutions.
\end{enumerate}

\subsection{Experiments and Results}

\begin{table}[ht]
\begin{center}
\vspace{-1em}
\subtable[]{\label{table:maxsat_firstsol}
\begin{tabular}{|l|r|r|}
\hline
\multicolumn{3}{|c|}{\textbf{D-Wave 2000Q}}\\\hline
%\textbf{Problem size}&\textbf{\# solved}&\textbf{\shortstack{\% optimal\\samples}}\\\hline
\textbf{Problem size}&\textbf{\# solved}&\textbf{\begin{tabular}{@{}c@{}}\% optimal\\samples\end{tabular}}\\\hline

\textbf{32 vars}&100&78.7\\\hline
\textbf{36 vars}&100&69.0\\\hline
\textbf{40 vars}&100&60.2\\\hline
\textbf{44 vars}&100&49.9\\\hline
\textbf{48 vars}&100&40.4\\\hline
\textbf{52 vars}&100&35.2\\\hline
\textbf{56 vars}&100&24.3\\\hline
\textbf{60 vars}&100&22.3\\\hline
\textbf{64 vars}&99&17.6\\\hline
\textbf{68 vars}&99&13.0\\\hline
\textbf{72 vars}&98&9.6\\\hline
\textbf{76 vars}&94&6.6\\\hline
\textbf{80 vars}&93&4.3\\\hline
\end{tabular}}
%\qquad
\subtable[]{\label{table:maxsat_firstsol_timing}
\begin{tabular}{|l|r|r|r|r|}
\hline
\multicolumn{5}{|c|}{\textbf{MaxSAT solvers: avg time (ms)}}\\\hline
\textbf{Problem
  size}&\textbf{g2wsat}&\textbf{rots}&\textbf{maxwalksat}&\textbf{novelty}\\\hline
%%RS: I've added some "0" decimals to have all with the same number of decimals
\textbf{32 vars}&0.020&0.018&0.034&0.039\\\hline
\textbf{36 vars}&0.025&0.022&0.043&0.060\\\hline
\textbf{40 vars}&0.039&0.029&0.056&0.119\\\hline
\textbf{44 vars}&0.049&0.043&0.070&0.187\\\hline
\textbf{48 vars}&0.069&0.054&0.093&0.311\\\hline
\textbf{52 vars}&0.122&0.075&0.115&0.687\\\hline
\textbf{56 vars}&0.181&0.112&0.156&1.319\\\hline
\textbf{60 vars}&0.261&0.130&0.167&1.884\\\hline
\textbf{64 vars}&0.527&0.159&0.207&4.272\\\hline
\textbf{68 vars}&0.652&0.210&0.270&8.739\\\hline
\textbf{72 vars}&0.838&0.287&0.312&14.118\\\hline
\textbf{76 vars}&1.223&0.382&0.396&18.916\\\hline
\textbf{80 vars}&1.426&0.485&0.430&95.057\\\hline
\end{tabular}} 
\caption{\label{table:maxsat}%
(a) Number of \maxsattoqubo{} problem instances (out of 100) solved by
the QA hardware %using 100 samples, 
and average fraction of samples
%from the QA hardware 
that are optimal.%solutions.  
\newline 
(b) Average time in \SI{}{\milli\second} taken to find an optimal solution by various inexact weighted MaxSAT solvers.%, averaged over 100 MaxSAT instances of each problem size.  
}
\end{center}
\end{table}
%
%\vspace{-1.0cm}
\begin{table}[h]
\begin{center}
\subtable[]{\label{table:maxsat_cutoff_dw}
\begin{tabular}{|l|r|r|}
\hline
\multicolumn{3}{|c|}{\textbf{D-Wave 2000Q}}\\\hline
\textbf{Size}&\textbf{anneal only}&\textbf{wall-clock}\\\hline
\textbf{32 vars}&448.5&443.9\\\hline
\textbf{36 vars}&607.0&579.9\\\hline
\textbf{40 vars}&1007.9&922.0\\\hline
\textbf{44 vars}&1322.6&1066.6\\\hline
\textbf{48 vars}&1555.4&1111.8\\\hline
\textbf{52 vars}&3229.0&1512.5\\\hline
\textbf{56 vars}&2418.9&1147.4\\\hline
\textbf{60 vars}&4015.3&1359.3\\\hline
\textbf{64 vars}&6692.6&1339.1\\\hline
\textbf{68 vars}&6504.2&1097.1\\\hline
\textbf{72 vars}&3707.6&731.7\\\hline
\textbf{76 vars}&2490.3&474.2\\\hline
\textbf{80 vars}&1439.4&332.7\\\hline
\end{tabular}}
\qquad
\subtable[]{\label{table:maxsat_cutoff_classical}
\begin{tabular}{|l|r|r|r|r|}
\hline
\multicolumn{5}{|c|}{\textbf{MaxSAT solvers}}\\\hline
\textbf{Size} &\textbf{g2wsat}&\textbf{rots}&\textbf{maxwalksat}&\textbf{novelty}\\\hline
\textbf{32 vars}&448.5&448.5&448.5&448.5\\\hline
\textbf{36 vars}&607.0&606.9&606.9&606.8\\\hline
\textbf{40 vars}&1007.7&1006.3&1005.3&1005.0\\\hline
\textbf{44 vars}&1313.8&1307.1&1311.7&1255.5\\\hline
\textbf{48 vars}&1515.4&1510.7&1504.9&1320.5\\\hline
\textbf{52 vars}&2707.5&2813.0&2854.6&1616.2\\\hline
\textbf{56 vars}&2021.9&2106.2&2186.6&969.8\\\hline
\textbf{60 vars}&2845.6&3061.7&3289.0&904.4\\\hline
\textbf{64 vars}&3100.0&4171.0&4770.0&570.6\\\hline
\textbf{68 vars}&2742.2&3823.3&4592.4&354.8\\\hline
\textbf{72 vars}&1841.1&2400.2&2943.4&212.6\\\hline
\textbf{76 vars}&1262.5&1716.0&2059.2&116.4\\\hline
\textbf{80 vars}&772.2&1111.1&1363.9&66.7\\\hline
\end{tabular}}
\caption{\label{table:maxsat_numsols}%
Distinct optimal solutions found in 1 second by various MaxSAT solvers, averaged across 100 instances.  ``anneal only" accounts for only the \SI{10}{\micro\second} per sample anneal, while ``wall-clock" accounts for the full time, including programming and readout. \newline(b) Classical computations were performed as in Table~\ref{table:maxsat}\subref{table:maxsat_firstsol_timing}.}
\end{center}
\end{table}



The problems are encoded with the same divide-and-conquer method, using exact penalty functions. As discussed, finding analytically the smallest approprate chain gap for encoding is unfeasible. Chain gaps that are too small result in a large number of broken chains, while high gaps reduce excessively the relative constraint below the noise level. For this experiment the optimal gaps have been found experimentally by sweeping over a range of values and choosing the best.  The chosen chain gap was always in the range $g_{chain} \in [2,6]$, relative to normal exact penalty functions (Section \ref{sec:foundations}).


The D-Wave processor is used to generate a single optimal MaxSAT solution
and Table \ref{table:maxsat} summarizes the results. Annealing was
executed at a rate of \SI{10}{\micro\second} per sample, for a total
of \SI{1}{\milli\second} of anneal time per instance. Again, the run-times for
various high-performing SLS MaxSAT solvers are added. Classical computations were performed on an Intel i7 2.90GHz $\times$ 4 processor. The solvers gw2sat\ignoreinshort{~\cite{li2005g2wsat}}, rots\ignoreinshort{~\cite{smyth2003rots}}, and novelty\ignoreinshort{~\cite{mcallester1997novelty}} are as implemented in \textsc{UBCSAT}~\cite{tompkins_ubcsat:_2004}. 
%All classical algorithms are performed with the optimal target weight specified; in the absence of a target weight they are much slower. 
The QA hardware solves
almost all problems with 100 samples/instance (i.e. within \SI{1}{m\second} of
anneal time). 

Table~\ref{table:maxsat_numsols} considers instead the performance in
generating distinct optimal solutions. For each solver and problem size, the
table indicates the number of distinct solutions found in 1 second, averaged
across 100 problem instances of that size. For the smallest problems, 1 second
is sufficient for all solvers to generate all solutions, while the diversity of
solutions found varies widely as problem size increases. The D-Wave processor
returns less optimal solutions for MaxSAT instances compared to the SAT
instances, but it is still effective in providing distinct optimal solutions
due to the rapid sampling rate.



\section[SGEN Problems on Pegasus]{SGEN Problems on Pegasus\sectionmark{sgen on pegasus}}
\sectionmark{sgen on pegasus}

All the previous experiments have been performed on the currently available
hardware, that uses the Chimera topology. Whereas it is not yet possible to run
the same experiments on the improved Pegasus topology, we can analyze the
impact of the new architecture by checking the maximum size that would fit for
the same problem class.

Table~\ref{tab:pegasus-encoding} shows the maximum problem size that can be
encoded for Pegasus chips of different sizes. For reference the previous
experiment were run on a $16\times16$ Chimera grid with 2048 qubits. We can
notice that the new architecture allows to encode bigger problem with fewer
qubits, in particular a $6\times6$ Pegasus hardware with 720 qubits can contain
problems of size roughly equal to the D-Wave 2000Q hardware. Furthermore, larger
future Pegasus chips $12\times12$ and $16\times16$ can hold problems that
require hundreds of seconds and more to be solved by UBCSAT
(Figure~\ref{fig:asymp_sat}).

\begin{table}
  \centering
  \begin{tabular}{|r r r r|}
    \hline
    Pegasus & \# qubits & \sgen{} size & \# constrs \\
    \hline
  
    4x4 & 288 & 44 & 33\\
    6x6 & 720 & 88 & 66 \\
    8x8 & 1344 & 128 & 96 \\
    12x12 & 3168 & 212 & 159 \\
    16x16 & 5760 & 320 & 240 \\
    \hline
    \end{tabular}
    
  \caption{Maximum size of encoded \sgen{} problems on Pegasus topologies. In comparison, on a Chimera 16x16 having 2048 qubits, the maximum \sgen{} size is 80.\label{tab:pegasus-encoding}}
\end{table}
