\chapter{Encoding Larger formulas\label{chp:large}}
\chaptermark{large encoding}
\section{Introduction}

In Section \ref{sec:foundations} we pointed out that large Boolean functions cannot be encoded using the SMT technique described in the previous
chapter, as the number of constraints and variables in the model grows exponentially with the number of variables $n$ of the Boolean function.
Thus in this chapter we will describe the complete ``divide-and-conquer'' approach that will be used on complex SAT or maxSAT problems

This approach consists in pre-computing a library of encoded Boolean functions
using the techniques of the previous chapter, and rewriting an input
Boolean function $\Fx$ as a conjunction of pre-encoded components
$\bigwedge_{k=1}^K F_k(\xsk)$. The pre-computed penalty functions
$P_{F_k}(\xsk,\ask|\ts^k)$ for these components may then be combined using
chains as described in Section \ref{sec:foundations-embedding}.
In terms of effectiveness, this method has been shown  to
outperform other encoding methods when encoding Boolean circuits
(see also
 ~\cite{bian2014discrete,bian2016mapping,su2016sat}). 
The general schema of this
  approach is shown in Figure \ref{procgraph}.   

Again, we closely follow the steps of~\cite{bian17_frocos17extended}.  Each stage will be described in the next sections.

\begin{figure}[tbh]
  \input{figures/encoding_process.tex}
\caption{Schema of the divide-and-conquer encoding process. \label{procgraph}}
\end{figure}



\section{Pre-encoding}

Pre-encoding is performed ahead-of-time on a collection of small selected
Boolean functions. The goal of this phase is to generate a database of
efficient encodings for Boolean functions that appear commonly in input
functions. The AND gate is sufficient to encode all possible boolean functions
(see section \ref{sec:background_aig}), and we can add several gates, such as
all 2,3 or 4 input basic gates, half and full adders and many others. Finding
these encodings can be computationally expensive, but it needs only to be
performed once for each NPN-inequivalent Boolean function.

There may exist many different penalty functions for any Boolean function with
different trade-offs. Penalty functions with more ancilla have larger gaps, but
can result in longer chains, so choosing the best option is not trivial. A
reasonable heuristic is to choose the smallest penalty functions with the same
gap of a chain $g_{min}=2$.

We can improve encoding results using knowledge of the target hardware graph.
For example, a natural choice of pre-computed gates for Chimera graphs is the
set of Boolean functions that can fit in single 8-qubit tile. In particular,
all $3$-input, $1$-output gates (all $3$-feasible cuts) can be inserted in one
tile.


\section{Preprocessing}\label{sec:large-simpl}

Preprocessing, or Boolean formula minimization, consists of simplifying the
input formula $\Fx$ to reduce its size or complexity. While not strictly
necessary, it not only improves QA performance by reducing the size of $\Pxa$
but also reduces the computational expense of the encoding process.

In Section \ref{sec:background_aig} we introduced the and-inverter graph
representation of circuits. Most of the state-of-the-art in preprocessing uses
AIGs as data structure to handle Boolean formulas (see Section
\ref{sec:background_aig}). Preprocessing is a well-studied problem, and mature
algorithms are available~\cite{Mishchenko06dag-awareaig,mishchenko2005fraigs}.
For our purposes we will use \textbf{DAG-aware minimization} as implemented by
the logic optimizer ABC~\cite{brayton2010abc}.

DAG-aware minimization attempts to find an AIG equivalent to the original with
a minimal number of nodes by repeatedly identifying small sub-graphs that can
be replaced with a smaller sub-graph without affecting the output. DAG-aware
minimization identifies a $4$-feasible cut $C$ for a node and replaces the
subgraph induced by $C$ with the smallest subgraph representing the same
Boolean function. There are $222$ NPN-inequivalent $4$-input Boolean functions,
a small enough number to be checked exhaustively. See~\cite{een2007applying}
for more details.

\section{Standard cell mapping}

The goal of the standard cell mapping phase is to decompose the
previously-simplified function $\Fx$ into component functions
$\bigwedge_{k=1}^K F_k(\xsk)$ where $F_k$ is taken from the library of
pre-encoded functions. The simplest method is to build the library out of the
gates used in the desired formula, so to ensure each $F_k(\xsk)$ is found in
the library possibly decomposing missing function into simpler components.
However, there are more advanced techniques that have been devised for digital
logic synthesis. \textbf{Technology mapping} is the process of mapping a
Boolean circuit to a network physical gates that can be placed in a digital
circuit~\cite{een2007applying,mishchenko2005technology}.


A technology mapping algorithm takes as input the library gates $F_k(\xsk)$,
each with an associated cost, creates a Boolean gate network that is equivalent
to $\Fx$ and attempts to minimize the total cost of the components in
$\bigwedge_{k=1}^K F_k(\xsk)$. When applied to digital circuits, technology
mapping is often used to reduce issues such as chip are a used, circuit delay
and load, and takes them into account in the cost calculation. Delay and load
do not play a role in the context of QAs, while area minimization implies
minimization of qubits used in the encoding, and is thus important to increase
effectiveness and simplify the subsequent placement and routing phase. We
define the cost of a gate $F_k$ to be the number of qubits used by the penalty
model $P_{F_k}$, so that the total cost $\Fx = \bigwedge_{k=1}^K F_k(\xsk)$ is
the number of qubits used to represent $\Fx$ before adding chains.

Here, we apply the technology mapping algorithm outlined in
\cite{een2007applying}. The technique relies on the definition of $k$-feasible cuts
outlined in Section~\ref{sec:background_aig}. Let be $\Fx$ a Boolean formula in AIG form
$D$. A \textbf{mapping} $M$ of an AIG $D$ is a function that maps every node
$a_i$ of $D$ to a $k$-feasible cut $M(a_i)$. We say $a_i$ is \textbf{active}
when $M(a_i)$ is not trivial and \textbf{inactive} otherwise. A mapping $M$ is
\textbf{proper} if:

\begin{enumerate}
\item every output $a_o$ of $D$  is active;
\item if $a_i$ is active, then every $a_j \in M(a_i)$ is active;
\item if $a_j$ is not an output and $a_j \not\in M(a_i)$, then $a_j$ is inactive.
\item for each active node $a_k$ there is a Boolean function $F_k(\zsk)$ represented by the cut $M(a_k)$
such that $F_x(\zsk)$(or a NPN-equivalent) appears in the pre-computed library.
\end{enumerate}

Each active node $a_j$ will become the output of a gate in our library, while
$M(a_j)$ will become its inputs. Thus, a successful mapping decomposes the
original Boolean function $\Fx$ in the following way (as in
Section\ref{sec:background-smt})

\[
\Fx \leftrightarrow \bigwedge_{k=1}^K F_k(\zsk) \wedge (a_o=\true).
\]

The simplest proper mapping is the trivial mapping, in which each $a_i$ is
mapped to the cut consisting of its two input nodes. The trivial mapping is
equivalent to a naive encoding of the function $\Fx$ using AND gates
(Example~\ref{es:exampleand} shows it can be trivially assumed that AND is in
the pre-computed library).


The algorithm in~\cite{een2007applying} iteratively improves mapping $M$ in the
following way. Each node $a_i$ is associated with a list $L(a_i)$ of
$k$-feasible cuts, ordered by cost. Traverse the graph from inputs $\xs$ to
primary output $a_o$. For each node $a_i$, recalculate the costs of the cuts in
$L(a_i)$ based on the costs of its children. Next, if $a_i$ is active and the
current cut $M(a_i)$ is not the cut in $L(a_i)$ of lowest cost, update
$M(a_i)$. To do this, first inactivate $a_i$ (which recursively inactivates
nodes in $M(a_i)$ if they are no longer necessary) and then reactivate $a_i$
(which reactivates nodes in $M(a_i)$, also recursively). The cost of a cut is
calculated using the \textbf{area-flow heuristic}. It is calculated by summing
the cost of using a particular gate, plus the best estimate cost of activating
all the nodes in the cut $M(a_i)$.


Finally, standard cell mapping algorithms typically take advantage of
NPN-equivalence of Boolean functions~\cite{mishchenko2005technology}, so the
library of available Boolean functions need only contain one representative
from each NPN-equivalence class.~\cite{mishchenko07lut}. This is done in a way
similar to the one described in Section~\ref{sec:foundations}.


\section{Placement and routing}\label{sec:large-placeandroute}

After the technology mapping phase $\Fx$ is decomposed into smaller functions
$\bigwedge_{k=1}^K F_k(\xsk)$, each with known penalty functions
$P_{F_k}(\xsk,\ask|\ts^k)$. The last encoding step provides the final variable
placement necessary to embed the formula onto the QA hardware as seen in
Equation \ref{eqn:distinct_qubits}. In Section
\ref{sec:foundations-embedding} we outlined various methods to perform
embedding. Here we will use the placement and routing technique. This process
has two parts: \textbf{placement}, in which each $P_{F_k}(\xsk,\ask|\ts^k)$ is
assigned to a disjoint subgraph of the QA hardware graph; and \textbf{routing},
in which chains are built in order to ensure that distinct qubits $x_i$ and
$x'_i$ representing the same variable take consistent values (using equivalence
constraints with penalty functions of the form $1-x_ix'_i$). Both placement and
routing are very well-studied in design of digital circuits~\cite{betz1997vpr}.
This stage is a computational bottleneck for encoding large Boolean functions.
Some placement and routing approaches have been outlined in Section \ref{sec:related-placeandroute}.

In Section \ref{sec:related-placeandroute} we have seen several approaches for placement and
routing, where the placement and routing stages of embedding are typically
performed separately. However, given the currently available Chimera and
Pegasus architectures with limited qubits a combined place-and-route algorithm
can be more effective~\cite{bian2016mapping}. The approach chosen here is using
a modified Bonn-routing with a custom heuristic for placement. As the placement
heuristic relies on routing, the latter algorithm will be described first.

First, routing in the context of embedding differs from the one in digital
circuit design, mainly because that vertices (qubits) are the sparse
resource that variables compete for, rather than edges. As a result,
vertex-weighted Steiner tree algorithms should be used rather than
edge-weighted ones and vertex-weighted
Steiner is harder to approximate~\cite{Byrka10,Klein95}. In practice, simple
algorithms for edge-weighted Steiner trees can be adapted to the vertex-weighted
problem. This section describes a modification of the routing algorithm
BonnRoute~\cite{Gester13} for vertex-weighted Steiner trees.

The first step is solve a continuous relaxation of the routing problem, called
\textbf{min-max resource allocation}. Given a set of vertices $C \subseteq V$,
the \textbf{characteristic vector} of $C$ is the vector $\chi(C) \in
\{0,1\}^{|V|}$ such that $\chi(C)_v = 1$ if $v \in C$ and $0$ otherwise. Let
$H_i$ be the convex hull of all characteristic vectors of Steiner trees of
$T_i$ in $G$. Then the {min-max resource allocation} problem for terminals
$T_1,\ldots,T_n$ is to minimize, over all $z_i \in H_i$, $i \in
\{1,\ldots,n\}$,

\[
\lambda(z_1,\ldots,z_n) \defas \max_{v \in V} \sum_{i=1}^n (z_i)_v.
\]

The vertices $v$ are the \textbf{resources}, which are allocated to \textbf{customers} $(z_1,\ldots,z_n)$ To recover the routing problem, note that if each $z_i$ is a characteristic vector of a single Steiner tree, then $\sum_{i=1}^n (z_i)_v$ the number of times vertex $v$ is used in a Steiner tree. In that case, $\lambda(x) \leq 1$ if and only if the Steiner trees are a solution to the routing problem. 

To solve the min-max resource allocation, first a weighted-Steiner tree approximation algorithm is used multiple times to approximate the convex hull. After each Steiner tree is generated, the weights of the vertices in that Steiner tree are increased to discourage future Steiner trees from reusing them (see Listing \ref{fig:Bonn} for details). The generated trees form  a probability distribution over the Steiner trees for each $x_i$. 

The BonnRoute algorithm produces good approximate solutions in reasonable time. More precisely, if  vertex-weighted Steiner tree approximations are approximated within a factor $\sigma$ of optimal, for any $\omega>0$ Listing \ref{fig:Bonn} computes a $\sigma(1+\omega)$-approximate solution to min-max resource allocation problem using $O(( \log |V|) (n + |V|) (\omega^{-2} + \log \log |V|))$ tree approximations~\cite{Muller11}.

\begin{listing}
\begin{algorithmic}
\Require{Graph $G$, Steiner tree terminals $\{T_1,\ldots,T_n\}$, number of iterations $t$, weight penalty $\alpha > 1$}
\Ensure{For each $i$, a probability distribution $p_{i,S_i}$ over all Steiner trees $S_i$ for terminals $T_i$} 
\Statex
\Function{BonnRoute}{$G$,$\{T_1,\ldots,T_n\}$}
\For{each $v \in V(G)$}
	\State{$w_v \gets 1$}
	\EndFor
\For{each Steiner tree $S_i$ for terminals $T_i$, $i \in [n]$}
	\State{$z_{i,S_i} \gets 0$}
	\EndFor
\For{$j$ from $1$ to $t$}
	\For{each $i \in [n]$}
		\State{Find a Steiner tree $S_i$ for terminals $T_i$ with vertex-weights $w_v$}
	    \State{$z_{i,S_i} \gets z_{i,S_i} + 1$}
	    \State{$w_v \gets w_v * \alpha$ for all $v \in S_i$}
	    \EndFor
	\EndFor
\State{Return $p_{i,S_i} \gets z_{i,S_i}/t$}
\EndFunction			
\end{algorithmic}
\caption{BonnRoute Resource Sharing Algorithm~\cite{Gester13}.}
\label{fig:Bonn}
\end{listing}

Once a solution to the min-max resource allocation has been found, a solution to the original routing problem is recovered by randomized rounding on the probability distributions.

When applying routing to graphs with a Chimera or Pegasus topology we can exploit the symmetry within each unit tile. In these cases it is convenient to work with a \textbf{reduced graph} in which
the horizontal qubits in each unit tile are identified as a single qubit, and similarly for the vertical qubits. As a result the scale of the routing problem is reduced by a factor of $4$. This necessitates the use of vertex capacities within the routing algorithm (each reduced vertex has a capacity of $4$), and variables are assigned to individual qubits within a tile during a secondary, detailed routing phase. 

Given a partial embedding, it is possible to estimate the cost of placing a new
$F_k$ in a particular position, by checking the shortest distance that each new
chain would have to traverse. Together with an estimation of which sub-problem
contribute the most to a particular embedding, this allows us to improve a
placement using a \textbf{rip-and-reroute} technique~\cite{bian2016mapping}. In
rip-and-reroute, a constraint (usually the most expensive) is removed from the embedding
and moved into the best available spot. We can also make use of tabu list to
avoid repeating movements on a cluster of badly-placed gates.

For placement we use an iterative approach based on rip-and-reroute. The graph
of $F_k$ components is traversed breadth-first; Each element is placed using
the previously mentioned heuristic. For each new element placed, we try to
improve the placement early by performing a single rip-and-reroute step. The
choice of the first component to be placed can be random, or a metric can be
chosen. In the libraries of Chapter \ref{chp:implementation} the $F_k$ with the
highest betweenness~\cite{betweenness} is used so that at the beginning, the most
``central'' sub-problem is placed in a ``central'' spot in the hardware.
