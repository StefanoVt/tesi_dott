\chapter{Implementation\label{chp:implementation}}

\newcommand{\library}[1]{\texttt{#1}}

\section{Introduction}

All the steps outlined in the previous chapters have been implemented as
various Python programs and libraries. A first implementation targeted Python
version 2, then all the code has been ported to support Python 3 as well. Some
of the tasks can be computing intensive, so it is advisable to use the
\library{PyPy} implementation of Python as interpreter. As the most computationally
intensive step, placement and routing, is critical for overall speed, the
author wrote a high performance version of the algorithm in C++ using the Boost
libraries. Notice that the Boost Python library does not allow linking with
\library{PyPy}, thus it currently works with the standard \library{CPython} interpreter
only. All code is released under MIT license and available at \url{https://bitbucket.org/StefanoVt/}.

This chapter describes each library and
explains their implementation details. First the basic file formats are described, together with the relative handling libraries. Then the implementation of each step of Chapter~\ref{chp:large} is described.

\section{Basic libraries}
\subsection{SMT-lib}

SMT-lib is the standard format for interacting with SMT (and OMT) solvers. It
uses \textbf{S-expressions}, a structured data format inherited from lisp.
S-expressions consists of atoms that can be symbols or numbers or of lists of
sub-S-expressions separated by a space and surrounded by parentheses. SMT-lib
semantics are specified by the official standard~\cite{rantin-smtlib}.

\begin{listing}[h]
    \begin{minted}[frame=single]{lisp}
    (set-logic QF_UF)
    (declare-fun p () Bool)
    (assert (and p (not p)))
    (check-sat)
    \end{minted}
\caption{Examples of S-expressions from a SMT-lib 
file.\label{lst:SMTlib-example}}
\end{listing}


The most complete Python library for SMT-lib is \library{pySMT}, but it is
not suited for our goal because it does not support OMT and interaction with
SMT solvers is hard to customize. Rather than using \library{pySMT} I wrote a
simpler library, called \library{smtutils}, that allows to transform python
expressions into SMT-lib S-expressions and present a simple wrapper to call a
SMT solver as an external process. Listing \ref{lst:SMTlib-example} shows a
small example of using \library{smtutils} for producing SMT-lib formulas and
calling a solver. Python expression are automatically converted to
S-expressions, and a simple wrapper spawn a SMT solver subprocess, sends the
formula to it and waits for the solution.

\begin{listing}[h]
    \begin{minted}[frame=single]{python}
    from smtutils.process import Solver, get_msat_path
    from smtutils.formula import SmtFormula, Symbol
    from smtutils.parsing import SmtResponseParser

    # Create a new formula
    formula = SmtFormula()

    # Declare variables
    a = Symbol("Int", "a")
    b = Symbol("Int", "b")

    # Add assertions
    formula.assert_(a+b == 5)
    formula.assert_(a < 100)

    # Check satisfiability, retrieve model for a and b
    formula.check_sat()
    formula.get_values(a, b)
    
    # Print final SMT-lib formula to screen
    print(str(formula))
    
    # Run the solver
    solver = Solver(get_msat_path("optimathsat"))
    result =  solver.run_formula(f)

    # Parse the SMT solver response
    p = SmtResponseParser(res)
    print(p.result, p.model)
    \end{minted}    
    \caption{Example usage of the smtutils library.\label{fig:smtutils-usage}}
\end{listing}

\subsection{RBC library}

Another fundamental task is to store and manipulate Boolean formulas and
circuits. To handle that, I wrote a library called \library{pyrbc}. The main two ways to represent Boolean circuits are as a network of
functions (represented as truth tables) and as and-inverter graphs. Boolean
networks are \textbf{directed acyclic graphs (DAGs)} where nodes are Boolean
functions, and inputs and outputs are connected through edges. As we have seen, and-inverter
graphs can be considered Boolean networks as well, but they have a single node type and edges can be negated. In section \ref{sec:large-simpl} we have seen that the state-of-the-art of circuit
simplification uses and-inverter graphs. For reference, Figure \ref{fig:aig} shows a simple AIG.

Another important representation of Boolean circuits is as a \textbf{reduced boolean
circuit (RBC)}. These are similar to AIGs, but use two types of nodes, XOR and
AND. The characteristic of RBC is not a particular encoding format, but their
circuit simplification technique. While a RBC is being constructed, nodes are
stored in a hash table, so that identical sub-graphs can be de-duplicated. This
technique is simple and does not detect functionally-equivalent nodes but is
fast and effective. The \library{pyrbc} library handles AIGs using this reduction
technique.

As a file format for Boolean networks the code will use the BLIF
format~\cite{brayton1991blif}, while AIGs are usually stored using the AIGER
standard format~\cite{jacobs2014extended}. The BLIF format is a simple text
format generally used in digital circuit specification. It allows the
definition of function networks, both from a standard function library and as
truth tables. The AIGER format encodes and-inverter graphs, either as
binary (more concise and useful for larger circuits) or text (clearer for humans
to understand). Listings~\ref{lst:blif-format}~and~\ref{lst:aiger-format} compare the same circuit under the same format.

\begin{listing}[h]
\begin{Verbatim}[frame=single,commandchars=\\\{\}]
\PYG{k+kn}{.model} 74283.isc
\PYG{k+kn}{.inputs} W1 W2 W3 W4 W5 W6 W7 W8 W9
\PYG{k+kn}{.outputs} W32 W36 W37 W38 W39
\PYG{k+kn}{.gate} \PYG{n+nn}{gate10} B=W9 C=W8 D=W7 A=n15
\PYG{k+kn}{.gate} \PYG{n+nn}{gate10} B=n15 C=W6 D=W5 A=n16
\PYG{k+kn}{.gate} \PYG{n+nn}{gate10} B=n16 C=W4 D=W3 A=n17
\PYG{k+kn}{.gate} \PYG{n+nn}{gate10} B=n17 C=W2 D=W1 A=W32
\PYG{k+kn}{.gate} \PYG{n+nn}{gate14} B=n17 C=W2 D=W1 A=W36
\PYG{k+kn}{.gate} \PYG{n+nn}{gate14} B=n16 C=W4 D=W3 A=W37
\PYG{k+kn}{.gate} \PYG{n+nn}{gate14} B=n15 C=W6 D=W5 A=W38
\PYG{k+kn}{.gate} \PYG{n+nn}{gate14} B=W9 C=W8 D=W7 A=W39
\PYG{k+kn}{.end}
\end{Verbatim}
    \caption{Circuit representation in BLIF format. Here gate are specified in a separate file (see Listing~\ref{lst:genlib-file})\label{lst:blif-format}}
\end{listing}
\begin{listing}[h]
    
\begin{Verbatim}[frame=single,commandchars=\\\{\}]
\PYG{c+c1}{c comment lines start with a c}
\PYG{c+c1}{c 74283.aig}
aag 41 9 0 5 32
\PYG{c+c1}{c list of inputs}
2
\PYG{c+c1}{c [...]}
18

\PYG{c+c1}{c list of outputs}
50
\PYG{c+c1}{c [...]}
82

\PYG{c+c1}{c list of gates}
20 5 3
22 4 2
24 9 7
\PYG{c+c1}{c [...]}
82 81 79
\end{Verbatim}
\caption{Circuit representation in  ASCII AIGER format.Gates are indexed by number $2n$ while $2n +1$ represent the negated $n$th gate.\label{lst:aiger-format}}
\end{listing}


The main components of \library{pyrbc} are the main nodes implementation, the node
database for RBC reduction and the import-export libraries for AIGER files and
external graph libraries. The main nodes are implemented with the goal of easy
initialization and manipulation. The database interface allows a
straightforward creation of graphs. It is possible convert the circuit into a
\library{networkx} graph in order to run graph algorithms, and read and write AIGER
files. Listing \ref{lst:pyrbc-example} shows an example of using \library{pyrbc} to read an AIGER file and traverse its nodes.

\begin{listing}[h]
\begin{minted}[frame=single]{python}
from pyrbc.aiger import parse_aig
from pyrbc.graph import build_graph
import networkx
import matplotlib.pyplot as plt

with open("example.aig", "rb") as f:
    outputs = parse_aig(f)

# Traverse the DAG, print all the nodes, avoid duplicates
visited = set()
for output in outputs:
    for node in output.iter_nodes(visited):
        print(node)

# Display the AIG using networkx
graph = build_graph(outputs)
networkx.draw(graph)
plt.show()
\end{minted}
\caption{Example of usage for the pyrbc library.\label{lst:pyrbc-example}}
\end{listing}
\subsection{GENLIB format}

Prepared gates are saved in a text file in GENLIB format. GENLIB is the main format used by ABC for storing the gates library for technology mapping. GENLIB file contain a text-base list of gates, with information such as name, formula representation, area used, pin maximum load and pin delays.


The format is tought for technology mapping of digital circuits, as it  contains information about electrical loads and delays that have no used for \sattoqubo{} encoding. In the same way the format does not contain info about the penalty function of a gate.
In order to maintain interoperability with ABC, the pre-encoding step produces a slightly modified format, where the penalty function of a gate is stored in JSON format in a GENLIB comment next to the description, and pin load/delay information is set to 0. Such a GENLIB file can be directly used to perform technology mapping using ABC without need to modify the software. Listing~\ref{lst:genlib-file} shows an example of such a format.

\begin{listing}

\begin{Verbatim}[frame=single,commandchars=\\\{\}]
\PYG{c+c1}{\PYGZsh{}  GATE gate_name gate_area gate_formula;\PYGZsh{} json_penalty_function} 
\PYG{c+c1}{\PYGZsh{}      PIN unused_pin_delay_information}

GATE gate10 6.000 A = (C + D) * (B + D) * (B + C);\PYGZsh{} \{"(bias ...
    PIN * INV 0 0 0 0 0 0

GATE gate14 8.000 A = (B + C + D) * (D + !B + !C) * (C + !B +...
    PIN * INV 0 0 0 0 0 0 
\end{Verbatim}

\caption{Gate specification in GENLIB format, with extra data in JSON format.\label{lst:genlib-file}}
\end{listing}


\subsection{Graph Algorithms}

Some graph algorithms are used in several contexts during the encoding process.
$k$-feasible cut (in short, $k$-cut) enumeration is an important step in technology mapping. The algorithm for $k$-cut enumeration has been outlined in Section~\ref{sec:background_aig}. This algorithm has been implemented in the \library{kcut} Python library, relying on the \library{networkx} graph library. It uses dynamic programming to avoid re-calculating cuts for the same nodes and it is fairly efficient.

Graph isomorphism is useful for symmetry reduction in SMT pre-encoding and during technology mapping. As hinted in Section~\ref{sec:NPN-placements}, the main tool for checking Graph isomorphism is the \textsc{Nauty} library, with a custom wrapper for ease of use with \library{networkx} classes.


\section{Pre-encoding}


\subsection{SMT encoding}

Given a Boolean relation as a Python function a penalty function can be sought using the \library{pfencoding} Python library. 
\library{pfencoding} provides some utilities to express penalty functions and
their constraints as Python and SMT-lib expression. It provides a
\library{PenaltyFunction} and a \\\library{MovablePenaltyFunction} to organize
the variables used in Equations~\ref{eq:encoding2-unrolled} and
\ref{eq:encoding3-penalty}. Furthermore a set of classes represents constraints,
such as \\\library{RangeConstraint} or \library{ArchitectureConstraint}. A class \\
\library{ExpansionGapConstraint} provides the constraints on the penalty
function obtained by Shannon expansion, while 
\library{VariableEliminationConstraint} provides an implementation of
Section~\ref{sec:foundations-ve}.

A utility module, called \library{search\_pf} aggregates these classes to provide several procedures, such as \library{search\_pf} that check if a penalty function exists, or \library{search\_pf\_smallest} that searches for the penalty function with the smallest number of ancillas. Listing~\ref{lst:pfencoding-example} shows an example of usage for the library.

\begin{listing}
\begin{minted}[frame=single]{python}
from pfencoding.searchpf import search_pf_smallest
from pfencoding.utils import print_pf
from networkx import complete_bipartite_graph

graph = complete_bipartite_graph(4,4)
nx = 4
na = 4

def and3(x):
    return x[0] == (x[1] and x[2] and x[3])


model, pf  = search_pf_smallest(nx, na, graph, and3, gap=2)

print(model)
if model:
    print_pf(pf, model, and3)
\end{minted}
\caption{Searching the smallest penalty function for the function $x_0 = (x_1 \wedge x_2 \wedge x_3)$ that fits inside a Chimera tile using \library{pfencoding}. \label{lst:pfencoding-example}}
\end{listing}


\subsection{Gate selection}

The gate that are chosen for addition generally depend on the problem domain
that is meant to be tackled. In general it is relatively simple to enumerate
small functions with up to 4 inputs~\cite{correia2001classifying}, and that
provides several gate when limiting oneself to functions fitting to a single
Chimera tile.

Alternatively the library \library{gatecollector} exploits $k$-cut enumeration to
collect the most frequent gates in a dataset of functions of AIG format. The
list of k-input function is put in a directory, ordered by decreasing
frequency. Listing~\ref{lst:gatecollector} provides a simple example of using the library for generating a list of gates out of a set of circuits..

\begin{listing}
\begin{minted}[frame=single]{python}
from glob import glob
from os.path import dirname
from gatecollector.encodeintiles import encode_aig,describe_aig
from gatecollector import database
from json import dumps

db = database.DelayedFunctionDatabase()
ngates = 100

for fn in  glob(dirname(__file__)+ "/dataset/*.aig"):
    #read all gates from file
    with open(fn, "rb") as f:
        db.read_function(f, 6)

# save to a file the most common gates
for i, gate   in enumerate(db.most_common(ngates)):
    (size, func, inputs), count = gate
    model = encode_aig(func)
    desc = describe_aig(func)
    if model:
        jsondata = {k: float(v) for k, v in model.items()}
        print ("""GATE gate{} {}.00 {};#{}
        PIN * INV 0 0 0 0 0 0 
        """.format(i, 
                model["ancilla_used"] + 1 + len(inputs), 
                desc, 
                dumps(jsondata)))
\end{minted}
\caption{Example of extracting the most common gates out of a dataset using \library{gatecollector}.\label{lst:gatecollector}}
\end{listing}

\section[Simplification and technology mapping]{Simplification and technology mapping\sectionmark{technology mapping}}
\sectionmark{technology mapping}


The most mature freely-available software for performing technology mapping is ABC. The software has a command line interface that allows the user to perform several operations on circuits. Listing~\ref{lst:ABC-techmap} shows an example of using ABC to perform simplification first and then technology mapping. ABC loads AIGER files for circuits and GENLIB files for pre-encoded gate library.  

\begin{listing}
    \begin{minted}[frame=single]{text}
UC Berkeley, ABC 1.01 (compiled Jan 27 2019 18:13:48)
abc 01> read ../datasets/74x/74283.isc.aig  
abc 02> read ../datasets/generated.genlib 
Entered genlib library with 34 gates from file "generated.genlib".
abc 02> source ../datasets/simplify.abc 
74283.isc: i/o = 9/5  lat = 0  and = 32  lev =  9
abc 211> source ../datasets/convert.abc
[...]  area = 56.00 lev = 4
abc 212> write 74283.blif
    \end{minted}
    \caption{Example of usage of ABC for technology mapping(edited for clarity).\label{lst:ABC-techmap}}
\end{listing}

However, ABC is is not tailored for penalty functions so I coded an alternative Python library to perform tech mapping, aptly named \library{techmapping}.
The libraries parses the genlib text databases, and replicates the basic algorithm used by ABC for technology mapping. This implementation is less effective than ABC in technology mapping, and thus the use of ABC is recommended.

The algorithm relies on $k$-cut enumeration and boolean matching. Given two
Boolean functions, the check for NPN-equivalence is called \textbf{Boolean
matching}. It is possible to perform Boolean mapping by reframing NPN-symmetry
as graph isomorphism. This allows the use of \textsc{Nauty} to check for NPN-equivalence
and furthermore it allows the creation of a NPN-canonical Boolean function. This
NPN-canonical function allows for an efficient Boolean matching of pre-encoded
libraries.


\section{Placement and Routing}

The placement and routing process as described in Section~\ref{sec:large-placeandroute} has been
implemented in another Python library called \library{placeandroute}. As this
step is the most computationally expensive, the core algorithm has been
re-implemented in C++.
 The library implements Bonn routing as described in the same section. It has been implemented both in Python and C++. 

The library works by setting an initial placement, finding the routing and then
applying different strategies for improving the placement in turn. First
several round of rip-and-reroute are applied, where badly-placed elements are
moved. When a certain number of round yield no improvement, a global rerouting
step is performed where all chains are removed and an alternative routing is
sought. The effort placed by the algorithm in finding a better encoding is
tuneable by the user. This effort includes precision in approximating Steiner
Trees during Bonn routing and number of rip and reroute attempts.

\subsection{Simplified Graphs and Detailed Routing}


In order to exploit symmetries in Chimera and Pegasus graphs placement and
routing are performed on simplified graphs. In Chimera a tile is compressed on
two nodes, each of capacity 4, while in Pegasus two adjacent nodes are paired
in a single node of capacity 2. To join a set of nodes into a single node it is
necessary that the nodes compressed together are perfectly interchangeable.
Possible placements are defined according on what class of hardware topology is
used and how single constraints are defined. In this implementation, a
placement is defined as a tile on Chimera and a 4-clique on Pegasus. In this
way constraints are placed in two nodes and a edge of the simplified graph.

This graph simplification is possible because a solution on the simplified
graph can be easily converted into a solution on the hardware graph (this is
possible when not considering missing qubits, but again merged nodes must be
perfectly interchangeable while missing qubits always break symmetries of the
hardware graph). The problem of finding a routing from the simplified graph is
called \textbf{detailed routing}. It is possible to show that for Pegasus and
Chimera the detailed routing problem is equivalent to interval graph coloring.
Thus if the simplified solution assigns to each simplified vertex a number of
variables that is lower than its capacity then a solution exists.

This property can be shown in the following way. Consider a solution on the
simplified graph where a variable $x_j$ is mapped to nodes $a$ and $b$ that are
connected by an edge Suppose that $a$ and $b$ represent merged nodes $A$ and
$B$ respectively . An issue in detailed routing arises only if there is no way
to pick qubits from $z_a \in A$ and $z_b \in B$ such that a chain can be
created. When $A \cup B$ is a complete bipartite graph we have no issues. $A
\cup B$ is not bipartite only along the vertical and horizontal couplings
between different tiles. Such connections form linear paths in the simplified
graphs of Chimera and Pegasus topologies. The detailed routing problem limited
to these paths is equivalence to the graph coloring problem on an interval
graph. Interval graph coloring is straightforward (using a greedy algorithm) and is guaranteed to find a solution with the maximum capacity.

This method can provide detailed routing when no qubits are missing in the hardware. If missing qubits are few enough not to break symmetries in the hardware graph (i.e. at most one for each row/column), they can be modeled as single-qubit chains, otherwise the greedy detailed routing can fail.





\subsection{Placement Heuristics}

The library allows many options for deciding an initial placement. The most
straightforward option is random initial placement; every gate is placed in a
random position and the task of finding a viable solution is left to the
placement improvement strategies.

The library provides an alternative initial placement procedure that reuses
Bonn routing and rip-and-reroute. Gates are selected using breadth-first search
on the Boolean network, starting with the most "central". Each selected element
is placed in turn using the heuristic of rip-and-reroute to find the best
candidate spot. Furthermore, each time that a new constraint is added, the
worst connected gate is moved using rip-and-reroute. This heuristic is quite
good in finding a placement but multiple round of rip-and-reroute can
significantly improve the placement.



\section{CLI scripts}
To use the libraries above, several scripts are available. Furthermore, two
scripts allow for generation of a pre-computed library in GENLIB format, and
one scripts perform a divide-and-conquer encoding of an AIG.


For preparing the pre-encoded library, the \library{preencode.py} script file
accepts as arguments the directory containing candidate gates as AIGER files,
and a output filename. It uses the \library{gatecollector} and
\library{pfencoded} library, essentially as shown in
Listing~\ref{lst:gatecollector}. The script produces a GENLIB file containing
the pre-encoded library. Listing~\ref{lst:preencode-usage} shows the usage
documentation.

\begin{listing}
    \begin{minted}[frame=single]{text}
usage: preencode.py [-h] [--output OUTPUT] [--numgates NUMGATES]
        [--cell {chimera,pegasus}]
        input_dir

Build a library of pre-encoded gates

positional arguments:
input_dir             directory containing circuits to be 
                                                      analyzed

optional arguments:
-h, --help            show this help message and exit
--output OUTPUT, -o OUTPUT
            GENLIB output file
--numgates NUMGATES, -n NUMGATES
            Number of gates to get (default: all)
--cell {chimera,pegasus}, -c {chimera,pegasus}
            type of cell (default: chimera tile)
    \end{minted}
    \caption{Usage documentation of \library{preencode.py}.\label{lst:preencode-usage}}
\end{listing}


Regarding the divide-and-conquer encoding, the \library{encode.py} script
perform the complete process. The script requires the input formula, the
pre-encoded GENLIB library, and the target hardware. First ABC is called to
perform preprocessing and technology mapping, then the result is parsed and
placed on the selected hardware. The encoded problem is returned as an array of
biases and couplings that is ready to be sent to the hardware using D-Wave API
libraries. Listing~\ref{lst:encode-usage} shows the usage documentation for the script.


\begin{listing}
\begin{minted}[frame=single]{text}
usage: encode.py [-h]
        [--hardware {chimera8,chimera12,chimera16,
                        pegasus6,pegasus8,pegasus12}]
        input library

Encode a SAT problem into a QA model.

positional arguments:
  input                 input SAT problem as AIG file
  library               pre-encoded library as GENLIB file

optional arguments:
  -h, --help            show this help message and exit
  --hardware {chimera8,chimera12,chimera16,
                pegasus6,pegasus8,pegasus12}
        -g   {chimera8,chimera12,chimera16,
                pegasus6,pegasus8,pegasus12}
                        QA hardware (default: chimera16)
    \end{minted}
    \caption{Usage documentation of \library{encode.py}.\label{lst:encode-usage}}
\end{listing}
