\chapter{Motivations and Goals}
\chaptermark{Motivations}\label{chp:motivations}

Quantum computing research has been thriving over the last decades since its
inception. This chapter is dedicated to explaining the reason for this
interest, what is its potential advantage over currently available computers,
and what are the obstacles that need to be overcome. First it will introduce
the context of quantum computation and computational complexity. Then it will
outline the computational advantages of quantum computing, and its practical
limits. Finally, the chapter will focus on the particular branch that the
thesis will contribute to and on which are the main obstacles to be overcome.

To understand the interest in quantum computing we need to consider its context
in the field of complexity theory. Complexity theory is concerned with the
asymptotic resource requirements necessary to solve a problem. One of the most
important results in complexity theory is NP-completeness and NP-hardness. These
concepts are fundamental and required for understanding quantum computing. In
the last 20-30 years there has been a development of the theory of quantum
computing complexity. This theory has important implications in computer
science and physics and opens new possibilities in computing.

Whereas the research on quantum computer technology still has not reached the
goal of manufacturing reliable large-scale devices, limited alternative models
have proven to be feasible. In particular, quantum annealing has shown an
advantage over the classical version, simulated annealing. We will see how
quantum annealing differs from gate-model quantum computers and what are the
limits of this approach.


\section[Intractable problems and NP-Hardness]{Intractable problems and NP-Hardness\sectionmark{intractable problems}}
\sectionmark{intractable problems}

Two of the most important concepts in complexity theory are nondeterministic
polynomial complexity and NP-hardness. The \textbf{Nondeterministic Polynomial
(NP)} class of problems consists in all the problems that have an algorithm
that, given a solution, are able to check it in polynomial time. If no better
algorithm is available, in the worst case every possible solution has to be
checked. A \textbf{NP-hard} problem is a problem such that an instance of any
problem in the NP class can be converted into an instance of the NP-hard
problem. Thus, an efficient solver for any single NP-Hard problem would be
capable to solve all NP problems as well. For this reason NP and NP-hard
problems are considered to be intractable, i.e. they do not have an efficient
algorithm. The proof of intractability of NP problems (the so-called $P$ vs.
$NP$ problem) is still an open problem, but intractability is strongly believed
due to various weaker theorems~\cite{fortnovpnp09}. All NP-complete and NP-hard
problems known so far have no efficient algorithm, and any algorithm found
would solve efficiently all the other problems. When we encounter NP-hard
algorithms in the real world, we rely on heuristics and approximations, and we
are not guaranteed to reach the solution of our problem in a reasonable time.

\begin{figure}[h]
    \centering
    \fbox{\includegraphics[width=0.7\textwidth]{figures/p_np_nphard.pdf}}

    \caption[Venn diagrams for complexity classes, if $P \not= NP$ or if $P = NP$.]{Venn diagrams for complexity classes, if $P \not= NP$ or if $P = NP$. \copyright Behnam Esfahbod, under  CC-BY-SA license. }
\end{figure}


Intractability is a problem because many real-world useful problems are
NP-Hard, from optimization, planning and artificial intelligence in general,
system analysis and many others. Almost all cryptography techniques rely on the
intractability of various NP algorithms. As an example that will be useful later, \textbf{Public Key
Cryptography} relies on the hardness of the factorization problem.
Factorization is in NP and no polynomial time algorithm is known, but it is not
believed to be NP-hard. The abstract nature of the NP-hardness definition
implies that problem complexity and intractability is independent of the
hardware used, as long as it follows the deterministic turing machine model.
This was the case for all known computer architectures until the development of
quantum computing theory. The non-determinism of quantum mechanics happens to
allow a more powerful model of computation.

\section{Quantum efficient problems}

The most surprising characteristic of the quantum mechanics theory is the
intrinsic non-determinism that it suggests. The laws of physics are generally
formulated in order to gain more knowledge about the world, but quantum
mechanics posits that there are some details of reality that are intrinsically
unknowable. It is an inherently probabilistic theory, unlike statistical
mechanics. In the classical statistical mechanics view of the world,
probability distribution is caused by the uncertainty of the observer. In
quantum mechanics we have superposition of states instead. Different world histories interfere with
each other. This leads to all kinds of counter-intuitive phenomena:
entanglement, tunneling. A notable example is the Einstein-Podoslky-Rosen
paradox, an experiment that disproves any hidden variable theory~\cite{selleri2013quantum}.

Quantum computing is a model of computation that relies on the weirdness of
quantum mechanics. We expect a computer to strictly follow a specific sequence
of instructions and acts on it internal state to reach a solution. This is the
deterministic model of computation. 
Non-deterministic automata instructions instead are partially defined, and
succeed when there exists a sequence of legal instructions that solve the problem.

As a quantum system is a superposition of classical states, a quantum computer
is in a superposition of states. While the evolution of a quantum system is
completely determined by its Hamiltonian function, Bell's theorem provides an
example of a quantum system which behavior cannot be described by a
deterministic local state (a local hidden variable). The behavior of quantum
computers lie in between deterministic and non-deterministic machines, more
powerful than deterministic, but bound by quantum-mechanic laws to less than
the vast possibilities of non-deterministic automata. The implications of this
will be explored further in the next chapter. The class of problems that are
efficiently solvable by quantum computers is called \textbf{bounded-error
quantum polynomial time (BQP)}. Various problems have been shown to have
efficient quantum algorithms. The performance of a quantum algorithm relative
to the best classical alternative is called quantum speedup. It is not
necessarily the case that the quantum algorithm is asymptotically better, but
quantum computing subsumes classical computing so it can only improve.

The most famous problem that is sped up by quantum computers is factorization,
which hardness underlies all public key cryptography schemes used nowadays on
the Internet. In 1994, Peter Shor showed that there is a quantum algorithm that
solves efficiently factorization\cite{shor1994algorithms}. Thus the ability to
manufacture a scalable quantum computer would compromise most current internet
security standard. As research makes quantum computing more and more feasible,
there is large interest for quantum-proof encryption, and work for
standardization is underway\cite{bookpostquantum,campagna2015quantum}.
On the upside, another important problem in BQP is simulation of quantum
systems\cite{deutsch1985quantum}. Quantum supremacy obviously implies that quantum systems are not efficiently simulable by classical computers. We would like to predict and analyze the behavior of quantum systems, and classical computer can do so with limited accuracy. An efficient quantum simulation would be extremely useful in science, especially material science. 

\begin{table}[]
    \begin{center}
       % \hspace{-6em}
\begin{tabular}{|c|c|c|c|}
    \hline
    Problem & \pbox{0.2\textwidth}{Classical \\ complexity} & \pbox{0.2\textwidth}{Quantum \\ complexity} & Quantum speedup \\
    \hline
    Factorization & $O(e^{(n \log n)^{{1}/{3}}})$ & $ O(n^2\log n) $ & exponential \\
    \hline
    Unstructured search & $O(2^n)$ & $2^{\sqrt{n}}$ & quadratic \\
    \hline
    Deutsch–Jozsa algorithm & $O(2^n)$ & $ 1 $ & exponential \\
    \hline
\end{tabular}
\end{center}
\caption{Complexity comparison between various algorithms, quantum speedups~\cite{jordan2018quantum}.\label{tab:qspeedups}}    
\end{table}

Still, a quantum computer is not as powerful as a generic non-deterministic
Turing machine. \textbf{Grover's algorithm}~\cite{grover1996fast} provides a
brute-force search that improves worst case complexity from $O(2^n)$ to
$O(2^{\sqrt{n}})$, and subsequent work has proved that this is the best possible
quantum algorithm for unstructured search\cite{bennett1997strengths}. In this
case then the best result we can get is only a quadratic speedup, which is still worse than a non-deterministic Turing machine that is exponentially faster.


The optimality of Grover's algorithm implies that exponential speedup can be
obtained only when the problem has an underlying structure that can be
exploited. For example, factorization can be sped up because it is a special
case of the so-called hidden-subgroup problem. The hidden-subgroup problem
consists in finding an algebraic group hidden in a bigger algebraic structure,
and quantum computers can solve it efficiently in the case of finite Abelian
groups. Table \ref{tab:qspeedups} shows a sample of algorithms and their best-known complexities on classical and quantum computers.


\section[Noise and decoherence in quantum computing]{Noise and decoherence in quantum computing\sectionmark{noise \& quantum computing}}
\sectionmark{noise \& quantum computing}
Whereas quantum computing theory has grown considerably and presented many useful
novel algorithms, the construction of an actual quantum computer has proved to
be a challenge. As we will see later, quantum computing assumes a noiseless
computing hardware, and by itself has no provision for noise tolerance. Thus,
quantum computers are really sensitive to noise, as any interaction with
environment causes \textbf{decoherence}. A system that fully loses coherence
becomes equivalent to a non-quantum system in a random state. Indeed a topic of
great interest in quantum computing and quantum communication is error
resilience. Some solutions are quantum error correction or topological quantum
computing.

\begin{figure}[h]
    \centering{
    \input{figures/quantum_error_correction.tex}
    }
    \caption{An example of quantum circuit, where the input is encoded. Each logical qubit is represented as group of three physical qubits (on the right), and is error corrected after the operation.\label{fig:quantum_error_correction}}
\end{figure}

\textbf{Quantum error correction} subsumes standard error correction schemes in
order to be able to cope with corruption of quantum states. A system of multiple
qubits is used to resiliently store a single logical qubit. An illustration of
a circuit with a simple error correction mechanism is available in Figure
\ref{fig:quantum_error_correction}. Before the computation happens, a single
logical qubit is encoded into multiple qubits and after computation any error
or change is corrected. If the error level in the underlying system is below a
certain threshold, one can build a error correction system with arbitrarily low
error rate\cite{PhysRevA.80.052312}. It is estimated that thousands
of qubits are necessary for error-correcting a single logical qubit\cite{Campbell2017}.
Whereas the technology is bound to improve, error-free quantum computation
needs to attain significant manufacturing improvements before being possibly
realizable.

%\begin{ignore}
%another interesting research path is topological quantum computing. This
%technique encodes quantum information to topological properties of a material,
%in such a way to not be affected by deformations. Currently proposed
%topological computers are fairly limited. For more information xxx cite.
%\end{ignore}

Currently available quantum computing hardware is still too noisy to perform
significant error-corrected quantum computation. An alternative approach is
instead to focus on exploiting quantum systems that are noisy and decohere
quickly: a way to do that is using a process called \textbf{Quantum Annealing}.
Quantum annealing is the quantum analogue of simulated annealing, a standard
algorithm in optimization. Simulated annealing mimics the annealing process in
physics in order to optimize a desired function. It is often effective because
it provides a simple compromise between exploitation and exploration, but of
course it is likely to get stuck on sub-optimal solutions on hard problems.
Quantum annealing is essentially a quantum system that undertakes an annealing
process. it is less likely to get stuck in higher-energy states due to the
\textbf{tunneling effect} even if the system loses coherence before the end of
the process.

\section[Adiabatic Quantum Computing and Quantum Annealing]{Adiabatic Quantum Computing and Quantum Annealing\sectionmark{aqc and annealing}}
\sectionmark{aqc and annealing}

Quantum annealing is closely related with the theory of adiabatic quantum
computing. \textbf{Adiabatic quantum computing} is an alternative model of
quantum computing that exploits the quantum adiabatic theorem. This theorem
ensures that a quantum system that evolves sufficiently slowly in time will
stay in the lowest energy state, called \textbf{ground state}. An adiabatic
quantum computer works by slowly transitioning a cold quantum system from a
standard initial state into a desired final state. The user programs the
computer by engineering the system in such a way that its final ground state
encodes the solution to the specified problem. The time necessary to perform
this transition is determined using the quantum adiabatic theorem, and depends
on the lowest energy gap between the ground state and the second lowest energy
state during the transition.

The minimum transition time represents the time required by an adiabatic
quantum computer to solve a problem. Because of this its estimation and
asymptotic growth has been the focus of researchers. Initially it was thought
to be more powerful than traditional quantum computing, but a careful analysis
proved worse performance when the annealing speed is assumed to be constant,
and equivalent with an adaptive annealing speed~\cite{albash2016adiabatic}.
Furthermore, a traditional quantum computer can be simulated with an adiabatic
one and vice versa, so they truly are two different interpretations of the same
computing model. Adiabatic quantum computing though has an advantage
considering that it takes noise in consideration: the minimum energy gap
separates the noiseless ground state from noise-excited states, and thus
ensures that there is no interaction with noise below a certain level. Thus,
compared to the gate model the adiabatic model includes a form of noise
resilience in its model. On the other hand, there is no known error correction threshold theorem for AQC.

%\subsection{Quantum annealing}

Adiabatic quantum computing assumes that the system never leaves the ground
state. This happens only when the system is at absolute zero and perfectly
isolated, or at least when it is isolated up to noise lower than the minimum gap.
In real systems though the temperature can never reach absolute zero, and noise
cannot be eliminated completely. Thus with the current technology we cannot
reliably ensure the adiabatic condition. In fact, current hardware still faces
significant problems in managing noise and large scale systems tend to lose
coherence very fast.

We can still exploit the fact that when the adiabatic condition is removed
the system still tends toward the lowest energy state in a process called
annealing. A quantum annealer still shows the presence of quantum effects but
loses coherence during the run. Quantum annealing accepts a loss of coherence
that is too big to perform adiabatic quantum computing, but still manages to
exploit partial coherence to perform tunneling. While simulated annealing
perform a stochastic search of the lowest energy state, a quantum annealer
tries to exploit coherence to explore a larger state space at the same time~\cite{amin15quasistatic}.

Quantum annealers, compared to the quantum gates model, have also a benefit in
their affinity to the simulated annealing algorithm. While quantum algorithm
like Shor's algorithm have no counterpart in classical computing, simulated
annealing is a widely used and established algorithm in stochastic
optimization. Thus simulated annealing can provide a clear reference for
evaluating performance and results of quantum annealers.


\section[Quantum Annealers and D-Wave]{Quantum Annealers and D-Wave\sectionmark{D-Wave QA}}
\sectionmark{D-Wave QA}

D-Wave Systems has built several quantum annealers. These machines consist in
Josephson junctions with tunable interactions. This kind of circuit
requires to be cooled to close to 0 degrees Kelvin, and are heavily shielded
against EM radiation.


\begin{figure}[H]
    \centering{
    \input{figures/dwave_annealer.tex}
    }
\caption{D-Wave's Quantum annealer. A picture of the refrigerator/shielding with a detail on the chip  and on the schema of a Josephson junction. Courtesy of D-Wave Systems Inc.}    
\end{figure}

The energy model of a D-Wave quantum annealer can be represented by a second
degree real polynomial in where qubit states can be either -1 or 1. Such type of  models is widely studied in Physics, and it is
known as \textbf{Ising model}.
The formula for this model is shown in equation \ref{eq:ising_model_intro}. In this model, $z_i$ are variables in $\left\{-1,\ 1\right\}$ and represent the state of a qubit. The parameters $\theta$ that influence the system's behavior are divided in three types: $\theta_0$ is called \textbf{offset}, the $\theta_i$ are called \textbf{biases} and the $\theta_{ij}$ are called \textbf{couplings}.

\begin{equation}\label{eq:ising_model_intro}
    P(z) \defas \theta_0 + \sum_i \theta_i z_i + \sum_{i,j} \theta_{ij} z_i z_j
\end{equation}



The problem of finding the ground state given of an Ising model is a particular
case of the so called \textbf{quadratic unconstrained binary optimization
(QUBO)} problem\footnotemark. It is easy to show that QUBO is a NP-hard problem by
reducing CIRCUIT-SAT (satisfiability of a Boolean circuit), a NP-complete
problem, into QUBO. We can translate each gate of a circuit into a simple
sub-problem and then compose them in a consistent way (we will see later how). Table
\ref{tab:satqubogates} shows simple translations for basic logic gates.

\footnotetext{Usually QUBO problems are stated on variables in $\{0,1\}$ rather than $\{-1,1\}$, which is an equivalent formulation}

\begin{table}[h]
    \centering
\begin{tabular}{| c | c | c |}
    \hline
Gate & Formula & QUBO polynomial \\\hline
AND &$x_3 = x_1 \wedge  x_2$ & $3 - x_3 - x_2 + 2x_1 + x_3x_2  - 2x_3x_1 - 2x_2x_1$ \\[1.5 ex]
XOR &$x_3 = x_1 \oplus  x_2$ & $ \begin{array}{c}5 + x_3 + a_2 - a_3 +\\ + x_1 a_1  - x_1 a_2 - x_1 a_3 - x_2 a_1 - x_2 a_2
- x_2 a_3 + x_3 a_2 - x_3 a_3\end{array}$ \\[3 ex]
NOT & $x_2 = \neg x_1$ & $1 + x_1x_2$ \\\hline
\end{tabular}
\caption{QUBO encoding of the basic logic gates. The QUBO polynomial is at its minimum value when the relation between variables is true. Notice how implementing the XOR gate requires adding two ancillary variables.\label{tab:satqubogates}}
\end{table}

D-Wave's machine has been demonstrated to have better performance than
simulated annealing for certain random QUBO problems
\cite{king2015benchmarking}, and has been used for solving traffic optimization
\cite{neukart2017traffic} and quantum simulation of material properties
\cite{king2018observation}. Using the naive conversion from SAT to QUBO instead
yields QUBO problems that are not suited to the annealer architecture, thus so
far naively-encoded circuits are too large or are very simple for a traditional
SAT solver.

\begin{figure}[h]
    \centering
    \includegraphics[width=1\textwidth]{figures/old/procroadmap.png}
    \caption{D-Wave annealers growth over the years. Courtesy of D-Wave Systems Inc.\label{fig:mooregrowth}}
\end{figure}

The number of available qubits in D-Wave's quantum annealers is growing in a Moore-like fashion. Figure
\ref{fig:mooregrowth} shows the history of D-Wave machine in the last years and
illustrates the phenomenon. This suggest an explosive growth of computing power
considering that the number of
qubits grows exponentially. In the latest years D-Wave put effort into improving the architecture
rather than just increase the number of qubits. The result is a newer,
better-connected architecture called Pegasus. We will see in the following
chapters the implications of having this improved architecture.

\section[Issues in Encoding for Quantum Annealers]{Issues in Encoding for Quantum Annealers\sectionmark{encoding issues}}
\sectionmark{encoding issues}

Quantum annealers are still hard to build and operate at large scale. While
naive conversion from circuit to QUBO is straightforward, the result is cannot
be passed to the quantum annealer directly. This is because so far:

\begin{itemize} 
\item The number of available qubits is limited.
\item The number of couplings is limited.
\item Noise and control precision limit the chances of success.
\end{itemize}

\paragraph{The number of available qubits is limited.} Even with 2000+ qubits,
the size of problems that can be solved is still small. This is because the
majority of qubits in quantum annealing has to be used to encode the problem,
unlike circuit models where the number of qubits represent the input width and
the circuit size are two different metrics for complexity. Consider a boolean
circuit that we want to check for satisfiability. In Grover search we need
enough gate to set up a superposition and run the circuit reversibly, and while
this is costly we can reuse qubits for intermediate values. When using quantum
annealing we need to encode the full circuit into a Ising model. Thus a deep
circuit using many levels with relatively few bits at a time (for example, a
cryptography primitive with a limited internal state and many computation
rounds) will require less qubits (ignoring the space requirements of gates).

\paragraph{The number of couplings is limited.} For a complete graph, the
number of possible couplings grows to the square of the number of qubits. While
difficult problems are not necessarily dense, a few qubits tend to have high
degree. In practice, each qubit can be coupled to a fixed low number of
neighbors, and the number of available couplings scales with the square root of the 
number of qubits. The problem encoding process then has to accommodate for the
couplings that are available. In general, there is a measure that correlates
directly with problem complexity\cite{Dechter1998}, called \textbf{tree-width}. It is a
measure of similarity to trees and will play an important role in the encoding
process. In Chimera and Pegasus, tree-width grows linearly with the number of
qubits.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{figures/chimera3x3_missingqb.pdf}
    \caption{A small Chimera annealer with missing qubits. A 3-by-3 grid of Chimera tiles, where the top-left and middle-right tiles have a damaged/unusable qubit. }
\end{figure}


\paragraph{Noise and control precision limit the chances of success.} Whereas the
D-wave machine is close to 0 K and heavily shielded, noise can still cause
performance degradation. In annealing the ground state of the annealer encodes
the best solution of our problem while higher energy states are less useful.
Thus we wish to have energy gaps between solution and non-solutions that are as
large as possible. Furthermore, obviously there are range bounds and
precision limits on the biases and couplings.

\section{Goals}

The concept of intrinsic greater power of quantum computers is generally called
\textbf{quantum supremacy}. Proving quantum supremacy with a real device is a
major goal of quantum computing research. To test for it we need problems that
are impossible for standard computers and easy for quantum ones. CSP and SAT
problems are very general problems, and tend to become very hard even for small
sizes. SAT problems have the potential to be small enough to fit in the
hardware but are still very hard for traditional computers.

D-Wave's quantum annealers have proven their strength for certain random QUBO problems
that match their architecture. Exploiting the annealer for generic problems is
less straightforward due to the need of encoding. Quantum annealing hardware is
expected to grow in a Moore-like fashion, so it will eventually reduce the
overhead, but we want to exploit in the best way the hardware we have now or in
the near future, and possibly to solve interesting problems with it. My
research goal is to pick a NP-hard problem and convert into an Ising problem in
an effective and efficient way.

\paragraph{Effective} means that the encoding process is capable to produce
encodings that fit in the available hardware, even when the input problem is
considerably big/difficult.

\paragraph{Efficient} means that the encoding process is reasonably fast to
perform, at least it has to be faster than actually trying to solve the problem
with a standard computer.

\paragraph{}First we will lay down a theoretical framework to approach the
encoding problem. We will frame it as a logic problem. This framework will
become the basis for the use of SMT solving techniques. We will then outline
the encoding strategy and describe in details each step. Then I will discuss
how to decompose the encoding process and how to perform each step in software.
After the process to encode a problem into a single quantum annealing problem, we need to
consider how to exploit quantum annealing. In practice we need to decompose our
problem into sub-problems that are solvable by the hardware and reasonably hard
for traditional computers. In later chapters we will see how it can be done.
Finally we will perform some preliminary evaluation to test the consistency of
the approach.