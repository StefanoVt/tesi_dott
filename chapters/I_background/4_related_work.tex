\chapter{Related Work}\label{chp:related-work}

In this chapter we will provide a brief overview of the literature pertaining quantum annealing and D-Wave's machines. Most of the literature is tangential to the \sattoqubo{} problem but provides a useful comparison. First  the chapter will outline how different problems have been encoded into Ising problems, then describe different techniques to  specifically make use of D-Wave hardware and finally will show some result reports on quantum annealing experiments.

\section[Combinatorial Problems and CSP Encoding]{Combinatorial Problems and CSP Encoding\sectionmark{CSP encoding}}
\sectionmark{CSP encoding}
There have been various previous efforts to map constraint satisfaction problems to Ising models~\cite{venturelli2015jobshop, rosenberg2015trading, dridi2016prime, perdomo2015quantum, rieffel2015planning, ogorman2016, zick15iso, bian13ramsey, jiang2018}. 
Most of those mappings have been for specific constraints types, but some were more systematic.

The core theoretical framework used in this thesis appeared in
\cite{bian2014discrete}, applied generic discrete optimization problems. The
paper introduces the concepts that will be outlined in the next chapters. In
particular it introduces the definition of penalty functions, the use of SMT
solvers and the variable elimination reformulation, and problem embedding. It
is focused on encoding specific constraints, in particular it provides a
specific example, a parity check problem. Figure~\ref{fig:bian14discrete} shows
the constraint used in the example, where exactly one out of 8 variables is
$\top$.

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth,trim=450 0 0 0,clip]{figures/related/bian2014.png}
    \caption{Encoding of an 1-in-8 CSP constraint found with a SMT solver, from~\cite{bian2014discrete}.\label{fig:bian14discrete}}
\end{figure}

A later paper by Bian et al.~\cite{bian2016mapping} applies the same approach
to \textbf{fault analysis} of Boolean circuits. This problem consists in the
following: given a Boolean circuit and a input-output pair, find the minimum
number of gates that are faulty. Again, the paper uses an approach very similar
to the one outlined in this thesis. Boolean circuits are encoded similarly but
the goal of fault analysis is different. Thus the paper introduces an
alternative definition of penalty function that is more suited to the task.
Rather than just finding a satisfiable solution, there is an interest in fair
sampling of possible solutions.


A paper by Lucas~\cite{lucas14ising} (see also~\cite{choi2011different}) provides encoding of various NP-hard
problems into Ising, among these is an encoding of the 3SAT problem. Given a
graph $G$, the \textbf{maximal independent set (MIS)} problem consists in
finding the greatest set $X$ of vertices such that no edge $(i,j)$ in $G$
contains both ends in $X$: $i \not \in X \vee j \not \in X$. The paper provides
the following Ising model encoding for the MIS problem, where $x_i = 1$ iff
$x_i \in X$.

$$ H = \sum_{i \in V_G} - x_i + \sum_{(i,j) \in E_G}  2 x_i x_j $$

Furthermore, it reports a trivial encoding of 3SAT to MIS: for each 3-clause,
add a 3-clique to $G$, where each node represents a literal. Then, for each
node representing literal $l$ add an edge to each node representing $\neg l$.
If a solution exists where at least one literal per clause is in the MIS $X$,
set that literal to $\top$; Otherwise, no satisfying assignment exists.
This encoding of 3SAT suffers from low effectiveness: three qubits are used for
each clause, plus a large amount of edges for each variable are added. The
result of the encoding then is usually large and hard to embed in the hardware.

A paper by Chancellor et al.~\cite{chancellor2016ksat} provides an encoding for the Max-k-SAT and low-density parity check problems. Two encodings are provided for two specific classes of constraints, disjunctions and parity checks.
Using these two constraints the paper proposes an encoding for the \textbf{Low Density parity problem}, used in efficient turbo codes. While heavily tuned and effective for the problem at hand, the two constraints are not extremely suited for generic SAT and maxSAT problems in general. The two encoding can be seen in Figure~\ref{fig:chancellor16}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/related/chancellor16.png}
    \caption{Induced graph for the encodings for the Max-4-SAT clause ($(x_1\vee x_2 \vee x_3 \vee x_4)$) and parity check ($(x_1\oplus x_2 \oplus x_3 \oplus x_4)$) from~\cite{chancellor2016ksat}.\label{fig:chancellor16}}
\end{figure}




      
Pakin in~\cite{pakin2016qmasm} provides a macro language to work with
constraint satisfaction problems. It implements a format to express Ising
models and libraries of penalty functions, and a software tool to handle them.
The software relies on D-Wave software libraries to perform the final embedding
step.


    
\section{Placement and Routing}\label{sec:related-placeandroute}
   
There are have been several approaches to map large Boolean functions or more generally large discrete optimization problems to fit D-Wave hardware. 
    
Most of these efforts have used global embedding (described in the next
chapters)~\cite{cai2014practical}, or otherwise, as in Trummer et
al.~\cite{trummer2016mqo}, Chancellor et al.~\cite{chancellor2016ksat},
Zaribafiyan et al.~\cite{zaribafiyan2016sys}, and Andriyash et al.~~\cite{
andriyash2017factoring}, used a ad-hoc placement approach optimized for the
specific constraints at hand.

Su et al.~\cite{su2016sat} instead used a different place-and-route approach,
based on simulated annealing of gate positions. The paper goal is to provide an encoding
for Boolean satisfiability problems. It uses a simple QUBO encoding, with a short list
of encoded two-input gates, and uses simulated annealing for placement and
routing. Table \ref{tab:su16} shows final results for embedding various
functions on a $100\times 100$ Chimera hardware graph, with a decisive
under-usage of resources.

\begin{table}[h]
    \centering
    \includegraphics[width=\textwidth]{figures/related/su16sat.png}
    \caption[Table of encoding results from~\cite{su2016sat}.]{Table of encoding results from~\cite{su2016sat}\label{tab:su16}. The columns contain, respectively: Name of the encoded problem, total number of qubits used for wires/chains, percentage of hardware cells/qubits used, and the run-time of the algoritm.}
\end{table}


\section{Performance Benchmarks}

Regarding D-Wave hardware performance, there have been several publications benchmarking the performance compared to software solvers.

McGeoch et al.~\cite{mcgeoch2013evaluation} and Santra et al.~\cite{Santra2014}
looked at (weighted) Max2SAT problems, comparing the state-of-the art with
quantum annealers. It is straightforward to convert Ising problem to Max2SAT
problems; Each term of a QUBO problem can be interpreted as a clause where the
$\theta$ parameter is the clause weight. Figure \ref{fig:mcgeoch13} plots the
relative performance between the quantum annealer and various software
algorithms.Another paper, by King et al.~\cite{king2015range}, found similar results for a class of constraint satisfaction problems.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.7\textwidth]{figures/related/mcgeoch13.png}
    \caption[Plotted success rates with a $491 ms$ second threshold for various solvers on various Max2SAT problems, from~\cite{mcgeoch2013evaluation}.]{Plotted success rates with a $491 ms$ second threshold for various solvers on various Max2SAT problems, from~\cite{mcgeoch2013evaluation}. \label{fig:mcgeoch13}. The graph compares the performance of various solvers (tabu,akmax,cplex) for the timescale of a quantum annealing process on a D-Wave machine (qa) and various software solvers. Larger problems require increasing amount of computation, while the quantum annealer finds optimal solutions for all problems in a single run (with $1000$ samples returned per run).}
\end{figure}

Douglass et al.~\cite{douglass2015filters} and Pudenz et al.~\cite{Pudenz2016}
looked at ALLSAT problems. The goal in these papers is to sample multiple diverse
solutions of a Boolean formula, in particular for the construction of SAT
filters. SAT filters are, similarly to Bloom filters, used to perform probabilistic membership queries on large sets.  Each element of a large set is mapped to a set of clauses for a SAT problem. One or more solutions of this SAT problem will function as filter. To query the filter for an element, we check if the previous solutions satisfy the mapped clauses. If not, the filter query returns a negative result. Thus, building a SAT filter requires finding a large number of solutions of a particular SAT
problem. Figure~\ref{fig:douglass15} shows the relative performance of D-Wave
annealer vs. various SAT solvers in SAT filter construction.
    
\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{figures/related/douglass15.png}
    \caption[Comparison of SAT filter performance of quantum annealing vs. various ALLSAT solvers 
    from~\cite{douglass2015filters}.]{Comparison of SAT filter performance of quantum annealing vs. various ALLSAT solvers 
    from~\cite{douglass2015filters}.\label{fig:douglass15} 
    %Different SAT filter implementations are compared,
    For comparison, the theoretical efficiency value for a Bloom Filter ($0.69$) is indicated (red line). For each solver, the graph shows performance for both off-line (upper, dashed lines) and on-line (lower, no lines) filters.  
    }
\end{figure}




For a more theoretical approach
Farhi et al.~\cite{Farhi2012} and Hen and Young~\cite{Hen2011} studied the performance of adiabatic quantum computing on several SAT and other intractable problems, using simulations to determine experimentally what are the run-times for reaching a solution. Both paper found that when the adiabatic quantum computer interpolates linearly between initial and final state the required time increased exponentially, even if with a lower coefficient than classical annealers.

