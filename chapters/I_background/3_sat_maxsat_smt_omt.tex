\section{SAT, MaxSAT, SMT and OMT} % [molto + espansa wrt. paper]


 \subsection{Basics}\label{sec:background-smt}
 In the following we recall the main concepts of the basic syntax, semantics and properties
 of Boolean and first-order logic and theories. We refer the reader to
 ~\cite{HandbookOfSAT2009,MSLM09HBSAT,LM09HBSAT,barrettsst09,sebastiani15_optimathsat}
  for more details.

Boolean logic deals with formulas over Boolean variables, variables that can
assume the value true ($\top$) or false ($\bot$). Given some finite set of
Boolean variables, or Boolean atoms, \xs{} the language of Boolean logic
(\calb) is the set of formulas containing the atoms in \xs{} and closed under
the standard propositional connectives \set{\neg,\wedge,\vee,\imp,\iff,\oplus}
(respectively called: NOT, AND, OR, IMPLY, IFF, XOR).

The AND ($\wedge$) operation is also called \textbf{conjunction}, OR ($\vee$)
is also called \textbf{disjunction} and NOT ($\neg$) is also called
\textbf{negation}. All other connectives can be defined in terms of
disjunction, conjunction and negation. The meaning of these connectives, i.e.
the value of the formula given the value of its variables, can be defined using
truth tables. A {\bf literal} is an atom, $x$ (positive literal) or its
negation $\neg x$ (negative literal). We implicitly remove double negations:
e.g., if $l$ is the negative literal $\neg x_i$, then by $\neg l$ we mean $x_i$
rather than $\neg\neg x_i$.

A formula is in \textbf{negative normal form (NNF)} if only AND and OR are used,
and negation appears only in negative literals. Every formula can be converted
into NNF using deMorgan's theorems. A {\bf clause} is a disjunction of
literals. A formula is in {\bf conjunctive normal form (CNF)} if it is written
as a conjunction of clauses. Conversely, a \textbf{cube} is a disjunction of
literals and a formula is in \textbf{disjunctive normal form (DNF)} if it is
written as a disjunction of cubes.

An assignment \xs{} {\bf satisfies} \Fx{} iff it makes it evaluate to
true. If so, \xs{} is called a {\bf model} for \Fx{}. A formula \Fx{} is {\bf
satisfiable} iff at least one truth assignment satisfies it, {\bf
unsatisfiable} otherwise. \Fx{} is {\bf valid} iff all truth assignments
satisfy it. $F_1(\xs), F_2(\xs)$ are {\bf equivalent} iff they are satisfied by
exactly the same truth assignments.


A formula \Fx which is not a conjunction can always be
decomposed into a conjunction of smaller formulas \Fxy{} by means of
\textbf{Tseitin's transformation}~\cite{tseitin68}, as in Equation
\ref{eq:decomposition}, where the $F_i$s are simple sub-formulas which decompose the
original formula \Fx, and the $y_i$s are fresh Boolean variables each labeling
the corresponding $F_i$.

  \begin{eqnarray}\nonumber
&\Fx = F_m(F_{m-1}(...(F_1(\vecof{x}))))\\
&\Fxy \defas \bigwedge_{i=1}^{m-1} (y_i \iff \Fixy) \wedge  \Fmxy \label{eq:decomposition}
\end{eqnarray}
  
Tseitin's transformation guarantees that $\Fx$ is
satisfiable if and only if \Fxy{} is satisfiable, and that if $\xs,\ys$ is a
model for \Fxy, then \xs is a model for \Fx. For this reason it is used
recursively for efficient CNF conversion of formulas~\cite{tseitin68}.


A {\bf quantified Boolean formula (QBF)} is an extension over the
aforementioned Boolean formulas. It is defined inductively as follows: a
Boolean formula is a QBF; if \Fx{} is a QBF, then $\forall x_i \Fx$ and
$\exists x_i \Fx$ are QBFs. QBFs can be converted to Boolean formula through
{\bf Shannon's expansion}: $\forall x_i \Fx$ is equivalent to $(\Fx_{x_i=\top}
\wedge \Fx_{x_i=\bot})$ and {$\exists x_i \Fx$} is equivalent to
{$(\Fx_{x_i=\top} \vee \Fx_{x_i=\bot})$}.

\subsection{And-Inverter Graphs}\label{sec:background_aig}

Any Boolean function can be represented as an \textbf{And-Inverter graph}. An
AIG, as the name suggests, is composed by $2$-input $\mathrm{AND}$ gates and
negations. More precisely, an AIG for $\Fx$ is a \textbf{directed acyclic graph (DAG)} $D$ on vertex set $\zs =
\xs \cup \vecof{g} = (x_1,\ldots,x_n,g_1,\ldots,g_m)$ with
the following properties:

\begin{enumerate}
\item Each $x_i$ has no incoming edges and each $g_k$ has $2$ incoming edges, and there is a unique $g_o$ with no outgoing arcs (the \textbf{primary output}). 
\item Each edge $z \rightarrow g$ is labelled with a sign $+$ or $-$ indicating whether or not $z$ should be negated as an input to $g$; define a literal $l_i(z) = z$ for an edge with sign $+$ and $l_i(z) = \neg z$ for an edge with sign $-$. 
\item For each node $g_k$ with edges incoming from $z_1$ and $z_2$, there is an $\mathrm{AND}$ function $A_k(g_k,z_1,z_2)~=~g_k \leftrightarrow l_{k}(z_1) \wedge l_{k}(z_2)$, such that 
\begin{equation}
\Fx \leftrightarrow \bigwedge_{k=1}^m A_k(\zs) \wedge (g_o=\true).
\label{eqn:and_gate}
\end{equation}
\end{enumerate}

 If is $\Fx$ is in CNF form, we can trivially construct an AIG by rewriting each $\mathrm{OR}$ clause as an $\mathrm{AND}$ function using De Morgan's Law, and then rewriting each $\mathrm{AND}$ function with more than $2$ inputs as a sequence of $2$-input $\mathrm{AND}$ functions. 

\begin{example}
The function 
$$\Fx = x_1 \wedge x_2 \wedge \neg x_3$$
is represented by both of the And-Inverter Graphs in Figure \ref{fig:aig}.
\end{example}

\begin{figure}[h]
  \centering
\includegraphics[scale=0.8]{figures/old/dag.pdf}
\caption{Two And-Inverter Graphs representing the function $\protect\Fx = x_1 \wedge x_2 \wedge \neg x_3$.}
\label{fig:aig}
\end{figure}


Let $G$ be an AIG with a node $z$. A \textbf{cut} $C$ of $z$ is a subset of
vertices of $G$ such that every directed path from an input $x_i$ to $z$ must
pass through $C$. The sub-graph of $G$ composed by all vertices that are
crossed by any path from $C$ to $z$ is effectively a AIG representation of $z$
as a function of $C$, since the Boolean value of $z$ is determined completely
by $C$. The equivalent function is the \textbf{Boolean function of $z$
represented by $C$}. Cut $C$ is \textbf{$k$-feasible} if $|C| \leq k$ and
\textbf{non-trivial} if $C \neq \{z\}$. For fixed $k$, there is a simple
linear-time algorithm to enumerate all $k$-feasible cuts in an AIG. Starting
from the inputs $\xs$ to the primary output, we can traverse the graph to list
the $k$-feasible cuts of node $a_i$ by combining $k$-feasible cuts of $a_i$'s
two inputs.


\subsection{SAT}

{\bf Propositional Satisfiability (SAT)} is the problem of establishing whether
an input Boolean formula is satisfiable or not. SAT is a NP-complete problem
\cite{Cook1971}. Not all SAT problem instances are hard: some restricted versions,
such as 2-SAT and HORN-SAT, are tractable.


Whereas it is implausible to find an algorithm that solves SAT problems beyond a
certain size in the worst case, a large amount of effort and ingenuity has been
put into speeding up resolution in the average case. Every year a competition
between the state-of-the-art solvers is held~\cite{satcomp}. In this competition
newer techniques and approaches are held in comparison. Efficient SAT solvers
are publicly available, most notably those based on {\bf Conflict-driven
clause-learning (CDCL)}~\cite{MSLM09HBSAT} and on {\bf stochastic local search}~\cite{Maj09HBSAT}. Most solvers require the input formula to be in CNF,
implementing a CNF pre-conversion based on Tseitin's transformation (Equation
\ref{eq:decomposition}) when this is not the case. See~\cite{HandbookOfSAT2009}
for a survey of SAT-related problems and techniques.


The most effective SAT solvers are based on CDCL. CDCL solvers are able to prove
the unsatisfiability of a formula, thus they are \textbf{complete} solvers.
CDCL solvers try partial assignments until a solution is found or when a clause becomes unsatisfiable. In the latter case, a conflict
analysis is performed after which a new clause is learned. This learned clause
reflects the latest decision done by the solver that is responsible for the
conflict. With this clause the solvers avoids future visit to the same
unsuccessful solution sub-space.

A different approach is to perform a random walk in the solution space. This is
the stochastic local search approach. This approach is very successful on large
random SAT problems but is not complete and thus it cannot prove the
unsatisfiability of a formula. SLS solvers start from a random assignment and
try to minimize the number of unsatisfied clauses. Various techniques are
employed to maximize state exploration and to avoid loops. SLS techniques are
closely related to simulated annealing approaches, though the latter are much
more general.

\subsection{MaxSAT}

\textbf{MaxSAT} is an extension of SAT, where we ask what model satisfies the
maximum amount of clauses of a CNF formula $F$ (that is typically unsatisfiable,
though a satisfiying model is a valid solution for a MaxSAT problem instance).
It is generally more useful to consider extensions of MaxSAT, such as weighted
MaxSAT and partial weighted MaxSAT. 
\textbf{Weighted MaxSAT} $\set{\tuple{F_k,c_k}}_k$ is an version of MaxSat such
that each clauese $F_k$ of $F$ is given a positive penalty $c_k \in
\mathbb{R}^+$ if $F_k$ is not satisfied, and an assignment minimizing the sum
of the penalties is sought. {\bf Partial Weighted MaxSAT} is a further
extension of Weighted MaxSAT such that some clauses, called {\bf hard
constraints}, must be satisfied, so they have penalty $+\infty$.


MaxSAT solvers rely heavily on SAT solution techniques. Efficient MaxSAT solvers
are publicly available (see, e.g.,~\cite{LM09HBSAT,tompkins_ubcsat:_2004}).
MaxSAT solver can use CDCL SAT techniques by using \textbf{core-guided} search:
when the SAT solver identifies a UNSAT core (i.e subset of clauses), the MaxSAT
solver bounds the upper cost of the search. SLS solvers can be extended to
Weighted MaxSAT by keeping account of the clause penalty during the
optimization search. SLS-based MaxSAT solver are penalized for Partial weighted
MaxSAT, as optimal model search cannot be trivially confined to models that
satisfy the hard constraints.


\subsection{SMT and OMT}\label{sec:background_smt}

{\bf Satisfiability Modulo Theories (SMT)} is another extension of SAT and a limited version of a first-order logic reasoning.
It consists in checking the satisfiability of first order formulas in a
background theory \T or in a  combinations of particular theories.
SMT solving is  focused on specific theories of interest, that generally have a specific decision algorithm.
  
For example, given \xs{} as in the previous section and some finite set of
rational-valued variables \vecof{v}, the language of the theory of {\bf Linear
Rational Arithmetic} (\larat) extends that of Boolean logics with
{\larat-atoms} in the form $(\sum_i c_i v_i \bowtie c)$, $c_i$ being rational
values, $v_i\in \vecof{v}$ and $\bowtie\ \in\set{=,\neq,<,>,\le,\ge}$, forming
linear algebraic expressions on the real numbers. 

In the theory of {\bf linear rational-integer arithmetic with uninterpreted
functions symbols} (\laeuf) the \larat language is extended by adding
integer-valued variables to \vecof{v} (\la) and {\bf uninterpreted function
symbols}. A n-ary function symbol $f()$ is said to be {\bf uninterpreted} if
its interpretations have no constraint, except that of being a function
(congruence): if $t_1=s_1$, ..., $t_n=s_n$ then
$f(t_1,...,t_n)=f(s_1,...,s_n)$. For example, $(x_i \imp (3v_1 + f(2v_2) \le
f(v_3)))$ is a \laeuf formula. Notice that the notions of literal, assignment,
clause and CNF, satisfiability, equivalence and validity, Tseitin's
transformation, quantified formulas and so on extend trivially to \larat and
\laeuf. \textbf{Satisfiability Modulo \laeuf{} (\smt{(\laeuf)})}
\cite{barrettsst09} is the problem of deciding the satisfiability of arbitrary
formulas on {\laeuf}. It is one of the most important combination of theories
and is extensively studied. Efficient \smt{(\laeuf)} tools are available,
including \mathsatfive~\cite{mathsat5_tacas13}.

SMT solvers rely on decision algorithms, one for each theory, and \textbf{theory combination} techniques to ensure the consistency of the model. Most modern SMT solver use lazy CDCL solving. During the CDCL search on the Boolean skeleton of the SMT formula, assertions on each theory are checked for consistency, and the theory solvers contribute to the conflict analysis.

\textbf{Optimization Modulo \laeuf{} (\omt(\laeuf))}
\cite{sebastiani15_optimathsat} is yet another extension of \smt{(\laeuf)}. In
\omt the goal is to search solutions which optimize some \la{} objective(s).
While in MaxSAT each clause has attached a particular penalty cost, in \omt the
cost is represented as a real-valued variable $c$. The focus on finding the
minimal model makes the problem more complex but enables further optimizations.
It is possible furthermore to have multiple cost variables $c_i$. In this case,
generally a \textbf{Pareto-efficient} solution is wanted. A solution is Pareto-efficient
if no single $c_i$ can be improved without worsening other $c_j$ costs.
Efficient \omlarat solvers, like \optimathsat~\cite{st_cav15}, are available on
the Internet.
