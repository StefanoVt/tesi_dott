\chapter{Background}

\section{Quantum computing}
We will start with a quick survey on quantum computing. Whereas the encoding process can consider the actual computation as a black box process, the theory behind quantum computation is useful to justify the context of the thesis and it can provide useful tools to interpret the results.
As the survey will be lacking in depth, the reader is invited to refer to Quantum Computation and Quantum Information\cite{nielsen2004quantum} for further details.

\subsection{Quantum mechanics}
\newtheorem{qpost}{Postulate}

The theory of quantum mechanics is based on the theory of complex linear
algebra. In this survey a basic knowledge of linear algebra will be necessary,
but most of the concepts will be explained as they appear. For the purpose of
the thesis the survey will assume only the finite dimensional spaces, as they
represent quantum system with discrete states. Generalizing results from
finite-dimensional to infinite-dimensional spaces is not trivial, but most of
the assertions in this survey are equally valid in the infinite-dimensional
case.

The theory of quantum mechanics can be stated with a series of four basic
postulates. These postulates will serve as a framework to write and reason
about quantum phenomena. The first postulate defines the state of a quantum
system:

\begin{qpost}
    Associated to any isolated physical system is a complex vector space
with inner product (that is, a Hilbert space) known as the state space of the
system. The system is completely described by its state vector, which is a unit
vector in the system’s state space.
\end{qpost} 

The basis of the Hilbert space will represent the set of possible outcomes,
i.e. the post-measurement classical states that we can observe. For example, a
system with two possible states (i.e. a qubit), the state of the system will be
represented as a two dimensional vector with two basis vector that we will
call, using the ket-notation, $\ket{0}$ and $\ket{1}$. We call a \textbf{pure
state} a unit vector (or, equivalently, a ray) in the Hilbert space that
represents a possible state of the system. We consider only unit vectors as
states as possible outcomes depend only on the relative amplitudes and phases
between vector components.



A pure state represents a perfectly known system; We can model uncertainty over
the quantum state with the so-called \textbf{mixed states}. A mixed state is a
distribution over superposition of states. It can be represented simply as a
density operator, a convex combination of multiple projective operators. Thus,
given possible states $\ket{\Psi_i}$ with probabilities $p_i$, the mixed state
$\rho$ of the system is:

\begin{equation}
    \rho = \sum_i p_i \ket{\Psi_i}\bra{\Psi_i}
\end{equation}

Two observations can be done on this formula. First, the density matrix for a
pure state is simply its projection operator $\ket{\Psi}\bra{\Psi}$. Then, in
the same way that $\sum_i p_i = 1$, we have that the trace of the density
operator $tr(\rho)$ is $1$.

The second postulate describes how a quantum system evolves over time:

\begin{qpost} 
    
The time evolution of the state of a closed quantum system is described by the
Schr{\"o}dinger equation,

\begin{equation}
i \frac{d\ket{\Psi}}{dt} = H \ket{\Psi}
\end{equation}

H is a fixed Hermitian operator known as the Hamiltonian of the closed system.
Then, the evolution of a closed quantum system is described by a unitary
transformation. That is, the state $\ket{\Psi_1}$ of the system at time $t_1$
is related to the state $\ket{\Psi_2}$ of the system at time $t_2$ by a unitary
operator U which depends only on the times t1 and t2.

\begin{equation}
    \ket{\Psi_2} = U \ket{\Psi_1} 
\end{equation} 

\end{qpost} 

Recall that an Hermitian operator is an operator $H$ that is its own dual ($H =
H^\dagger$) and always has a spectral decomposition ($ H = \sum_i e_i
\ket{E_i}\bra{E_i},\ e_i \in \mathbb{R}$), while a unitary operator is a
operator $U$ such that $U^\dagger U=I$. The spectral decomposition of the
Hermitian matrix in Schr{\"o}dinger equation has an important interpretation.
The eigenstates $\ket{E_i}$ are called \textbf{stationary states}, and the
eigenstate with lowest eigenvalue $e_0$ is called \textbf{ground state}. A
direct consequence of the spectral decomposition is that quantum states evolve
using unitary matrixes. If the quantum state is a unit vector, a quantum
computer instruction is a unitary operator.

We can easily generalize system evolution on a mixed state $\rho$ given the unitary operator $U$:

\begin{equation}
    \rho' = U \rho U^\dagger
\end{equation}


Notice how the postulate refers to an isolated system and a Hamiltonian fixed
in time. We generally want to interact with a system and vary its Hamiltonian.
For many such systems we can write a time-varying Hamiltonian, where the effect
of the environment on the system is represented with a changing Hamiltonian
$H(t)$:

\begin{equation}
    i \frac{d\ket{\Psi}}{dt} = H(t) \ket{\Psi}
\end{equation}

The third postulate describes how the quantum world interacts with the classical world, i.e. how measurements are made.

\begin{qpost}

Quantum measurements are described by a collection $\left\{M_m\right\}$ of
measurement operators such that:

\begin{equation}
    \sum_m M_m^\dagger M_m  = I
\end{equation}

These are operators acting on the state space of the system being measured. The
index $m$ refers to the measurement outcomes that may occur in the experiment.
If the state of the quantum system is $\ket{\Psi}$ immediately before the
measurement then the probability that result $m$ occurs and the state of the system after the measurement are given by:

\begin{eqnarray} 
&p(m) = \bra{\Psi} M_m^\dagger M_m \ket{\Psi} \\
&\ket{\Psi_m} = \frac{M_m \ket{\Psi}}{\sqrt{p(m)}} 
\end{eqnarray}

\end{qpost}

The postulate is the most general specification of measurement. In the simplest
case, called \textbf{projective measurement}, the $M_m$ operators can be mapped into projective operators for a particular basis of the Hilbert space:

\begin{equation}
     M_m^\dagger M_m = P_m = \ket{\phi_m}\bra{\phi_m}
\end{equation}
 
In the case that the measurement outcomes $m$ are real-valued, we can represent the measurement with a single matrix $M$, called \textbf{observable}. This form allows us to represent concisely the expected value of the measurement $\mathbb{E}_\Psi\left[ M\right ]$.:

\begin{eqnarray}
  &  M = \sum_m m\ket{\phi_m}\bra{\phi_m}\\
  &  \mathbb{E}_\Psi\left[ M \right ] = \left< \Psi | M \Psi \right >
\end{eqnarray}

We can generalize the measurement of a mixed state $\rho$ and get its
probability and post-measurement state:

\begin{eqnarray}
    p(m) = tr(M_m^\dagger M_m \rho) \\
    \rho_m = \frac{M_m^\dagger\rho M_m}{p(m)}
\end{eqnarray}

Notice how we have non-determinism in measurement. Even in a fully-known pure
state the outcome of a measurement can be random. Furthermore, measurement is a
destructive operation, as the original state is modified, and only projective
measurements are repeatable, i.e. applying the same measurement twice yields
the same result.


The last postulate describes the composition of quantum states:

\begin{qpost}
 
The state space of a composite physical system is the tensor product of the
state spaces of the component physical systems. Moreover, if we have systems
numbered 1 through n, and system number i is prepared in the state
$\ket{\Psi_i}$, then the joint state of the total system is $\ket{\Psi_1}
\otimes \ket{\Psi_2} \otimes ... \oplus \ket{\Psi_n}$.

\end{qpost} 

For example the composition of two one-qubit systems labeled $a$ and $b$, each
using the base $\ket{0},\ \ket{1}$, is a Hilbert space with basis:

\begin{eqnarray*}
    \ket{0_a}\otimes\ket{0_b} = \ket{00}\\
    \ket{0_a}\otimes\ket{1_b} = \ket{01}\\
    \ket{1_a}\otimes\ket{0_b} = \ket{10}\\
    \ket{1_a}\otimes\ket{1_b} = \ket{11}
\end{eqnarray*} 

It follows then that the number dimensions of a discrete Hilbert space doubles
for each added qubit. Thus, the representation of a quantum state in a
classical computer needs an amount of space that is exponential to the number
of qubits. This is one of the reasons for distinction from quantum and classical
computing, and what makes many-body matter simulations hard to simulate and
predict.

Another important consequence is that a two-qubit system can be put in
entangled state $\ket{\Psi} = \ket{00} + \ket{11}$. This state is peculiar
because the system cannot be expressed anymore as the composition of two
single-qubit systems: this is an \textbf{entangled state}.

\subsection{Qubits, quantum gates, adiabatic computing}

The basic unit of quantum information is a \textbf{qubit}, a discrete quantum
system with two possible states usually called $\ket{0}$ and $\ket{1}$. While a
bit is an element that can be in one of two states, a qubit is in a
superposition of these two states. A qubit state can be represented as a point
on the \textbf{Bloch sphere}. A pure state is a point on the surface while a
mixed state as a point inside its volume. 
Notice that this representation fails to generalize to multiple qubits.

\begin{figure}[h]
    \centering
    \begin{blochsphere}[radius=0.2\textwidth,tilt=15,rotation=-120, ball=circle]
        \drawBallGrid[style={color=gray, dashed}]{30}{30}
        \labelLatLon{up}{90}{0};
        \labelLatLon{down}{-90}{90};
        \labelLatLon{plus}{0}{-90};
        \labelLatLon{minus}{0}{90};
        \labelLatLon{fnt}{0}{0};
        \labelLatLon{bck}{0}{180};
        \node[above] at (up) {{ $\left|1\right>$ }};
        \node[below] at (down) {{ $\left|0\right>$}};
        \node[right] at (plus) {{ $\left|+\right>= \frac{\sqrt{2}}{2} \ket{1} + \frac{\sqrt{2}}{2} \ket{0}$ }};
        \node[left] at (minus) {{ $\left|-\right> = \frac{\sqrt{2}}{2} \ket{1} - \frac{\sqrt{2}}{2} \ket{0}$}};
        \node at (fnt) {{$\left|\uparrow\right>= \frac{\sqrt{2}}{2} \ket{1} + i\frac{\sqrt{2}}{2} \ket{0}$}};
        \node at (bck) {{$\left|\downarrow\right>= \frac{\sqrt{2}}{2} \ket{1} - i\frac{\sqrt{2}}{2} \ket{0}$}};
    \end{blochsphere}
    \caption{Representation of the state of a single qubit as a Bloch sphere. Any point on the surface of the sphere represents a pure state, and any point inside the sphere represents a mixed state.
    \label{fig:bloch-sphere}}
\end{figure}

Within the Bloch representation, unitary transformations are represented by 3d
rotations and reflections of the sphere. Thus, while the only single-bit functions are identity and negation, we are able to perform several several quantum operations on a single qubit. The simplest example is
negation (Equation \ref{eq:qt-negation}), other important operations are the
Hadamard gate (Equation \ref{eq:hadamard}) and the $\pi / 8$ half-phase gate
(Equation \ref{eq:phase-gate}).

\begin{eqnarray}
    X = 
        \begin{bmatrix}
            0 & 1\\
            1 & 0
        \end{bmatrix}
        \label{eq:qt-negation}\\
    H = \frac{\sqrt{2}}{2}
        \begin{bmatrix}
            1  & 1 \\
            1 & -1 
        \end{bmatrix}
        \label{eq:hadamard} \\
    T = 
        \begin{bmatrix}
            1 & 0 \\
            0 & e^{i\pi/4}
        \end{bmatrix}
        \label{eq:phase-gate}
\end{eqnarray}

The most important multiple qubit operation is the controlled-not gate (CNOT)
where the second bit is negated if the first is 1. The CNOT, Hadamard and $\pi
/ 8 $ half-phase gate are a universal set of gates~\cite{nielsen2004quantum}.
This means that we can approximate any quantum transformation on an arbitrary
number of qubits using only these gates. Figure \ref{fig:basic-qgates} shows
the canonical representations for these common gates.
These gates are fundamental for quantum computing theory, and by virtue of their
universality they can represent any computation done by quantum systems, but as
we will focus on quantum annealing, they will be of limited use.

\begin{figure}[h]
    \centering
    \input{figures/basic_quantum_gates.tex}
    \caption{Standard representation for the basic quantum gates (from left to right: negation, Hadamard, half-phase and controlled not).\label{fig:basic-qgates}}
\end{figure}


\iffalse
We want to avoid measurement, so we want to copy the output and uncompute. To
do that we need reversible gates, and the toffoli gate is a universal
reversible logic gate.

\begin{figure}
    \todofig{Uncomputation}
\end{figure}
\fi


\section[Adiabatic Quantum Computing and quantum annealing ]{Adiabatic Quantum Computing and quantum annealing \sectionmark{AQC andQA}}
\sectionmark{AQC and QA}

%\subsection{Adiabatic Quantum Computing}
\subsection{Adiabatic Quantum Computing}

The adiabatic quantum computing approach relies on the adiabatic quantum
theorem. It is a direct consequence of the time-dependent Shr{\"o}dinger
equation. The adiabatic theorem states that when the Hamiltonian changes slowly
enough over time, a quantum system that starts in the initial ground state ends
in the final ground state.

\newtheorem{theorem}{Theorem}
\begin{theorem}
    Given a quantum system at ground state $\ket{\Psi^i_0}$ and the time-varying Hamiltonian $H(t)= H_i (T-t) + H_f t$, given large enough transition time $T$ the system will stay in the ground state up to the final state  $\ket{\Psi^f_0}$.   
\end{theorem}       

As stated above, the theorem does not specify any explicit bound on $T$. In
general, if $g_{min}$ is the minimum energy gap between the ground state
$\ket{\Psi_0(t)}$ and the other stationary states the required time will be
proportional to the inverse of the square of $g_{min}$, but there are more
rigorous bounds on the transition time. AQC can efficiently simulate quantum
gates and vice versa: A quantum circuit can be encoded as the ground state of
a Hamiltonian, and the AQC Hamiltonian can be integrated to a unitary operator
that can be approximated by quantum gates. The reader that is interested in a
thorough exposition is suggested to consult~\cite{albash2016adiabatic}.


%\subsection{Annealing: Boltzmann statistics}


The quantum adiabatic theorem holds for a system that is already at the ground
state. If we switch point of view to thermodynamics, this state is at the
absolute zero temperature. In theory, in an adiabatic quantum system the quantized energy gaps theoretically ensures that a higher temperature requires a discrete amount of energy. 
In thermodynamics when the temperature is not zero we have a process
called annealing. Both in classical and quantum annealing the probability of a
certain state is governed by  Boltzmann statistics, stated in Theorem
\ref{thm:boltzmann}.

\begin{theorem}\label{thm:boltzmann}
    A thermodynamic system at equilibrium can be found in the state $x$ with the following probability:
    \begin{equation}
        p(x) = \frac{1}{Z}e^{\frac{E(x)}{kT}}
    \end{equation}
    where T is the temperature of the system, and Z is the partition function: $Z = \sum_x e^\frac{E(x)}{kT}$
\end{theorem}

The Boltzmann distribution is used widely to calculate properties of
thermodynamics systems. The most relevant observation for the thesis is that
when the temperature is high every state is equi-probable while when the
temperature is small low energy states are more probable. Simulated annealing
consists essentially in simulating a system moving toward equilibrium while
lowering the temperature. A low energy state consists in a solution with a low
cost according to the problem constraints.

Boltzmann statistics are valid for classical Ising model and quantum systems at
high temperature and low concentrations, and so are limited in explaining
quantum annealer, but are conceptually important when considering open quantum
systems that tend toward an equilibrium state~\cite{kulchytskyy2016qbm}.

\subsection{Ising models}

So far the Hamiltonian function that represent the energy landscape has been
not yet defined. The quantum annealers that will be used in this thesis will
have an Ising energy model. The Ising model is one of the most important models
in statistical physics. It has been used to study various phenomena in matter,
like magnetization.
 
In the Ising model the particles/elements can be only in two states, $-1$ or
$1$. The Hamiltonian is defined as a second degree real polynomial on binary
variables (Equation \ref{eq:backg-ising}). In this polynomial $\theta_i$
represent biases of a single element, while $\theta_{ij}$ represent the effect
of interaction between pairs of elements. 

\begin{equation}
    H(\zs) =  \sum_i \theta_i z_i + \sum_{ij} \theta_{ij}z_i z_j 
    \label{eq:backg-ising}
\end{equation}

The problem of finding the minimum of a polynomial over binary variables is
called \textbf{quadratic unbounded binary optimization (QUBO)}. A QUBO problem
ask, given a quadratic polynomial with binary variables what is its minimum
value assignment. In the QUBO literature it is usually assumed that variables
takes values in $\{0,1\}$ while Ising model variables take values in $\{-1,
1\}$, but the two representations are equivalent and conversion is trivial. If
the state of the system is expressed as a binary vector $\zs \in \{0,1\}^n$,
the QUBO problem derived by an Ising model can be expressed also as a quadratic
form (Equation \ref{eq:backg-ising-mat}). In this case the parameters are
represented with a single matrix $\Theta$.

\begin{equation}\label{eq:backg-ising-mat}
H(\zs) = \zs^T \Theta \zs 
\end{equation}



\subsection{Induced graphs and their properties}
\label{sec:background_graph}

Ising models are generally classified by the topology of the interaction.
Given a second degree polynomial we can define an \textbf{induced graph} where
vertices are variables/qubits, edges are non-zero second degree terms or
available couplings. The properties of the induced graph are important in terms
of complexity of the problem and useful for encoding.
Given a graph $G$, a graph minor is a graph where an edge or a vertex is removed, or
two vertices are merged. When $G$ is an induced graph, setting the value of a
variable or forcing equivalence between two variables is equivalent to removing
or merging vertices.

The shape of the induced graph of a QUBO problem affects its hardness.
We have seen that QUBO is a NP-hard problem in the general case, but it is not
trivial that an Ising model with a certain topology is NP-Hard as well, and indeed a planar graph with no biases are tractable~\cite{barahona1982computational}. Later we will see various method to reduce general QUBO problems into problems that have the induced graph of the quantum annealer hardware.

In later chapters we will make use of symmetries in induced graphs. A permutation
$\sigma: V \rightarrow V$ is an \textbf{automorphism} of a graph $G$ if relabeling its vertices
with $\sigma$ produces the same graph: $\sigma(G) = G$. Automorphisms form a
group (where the identity function is the identity and function composition is
the operator), called $Aut(G)$. When the group is associated with an action
$\phi(\sigma, x) : (Aut(G), X) -> X$ (see~\cite{dummit2004abstract}) the group
orbit of $x$ is the set of elements of $X$ that can be reached from $x$: $G
\times x = \{\forall g \in Aut(G): \phi(g, x) \}$. Group orbits form a
partition of $X$, and thus form an equivalence relation on it.

Another important graph property is \textbf{tree-width}. The tree-width encodes
the smallest node clustering that yields a tree. Given a graph $G= (V_G, E_G)$ a tree decomposition is a tree $T$ composed of nodes $X_1,...,X_n$ such that:

\begin{itemize}
\item  $X_1,...,X_n$ are subsets of $V_G$, and $\bigcup X_i = V_G$.
\item All the $X_i$ that contains a vertex $v$ form a connected subtree of $T$.
\item For every edge $(v,w)\in E_G$ there exists a $X_i$ that contains both $v$ and $w$.
\end{itemize}

The width of the decomposition is the maximum size of the $X_i$ minus 1, and
the tree-width of a graph is the minimum width for all possible decompositions
of a graph. Figure \ref{fig:treewidth} illustrates a simple tree decomposition of width 2. Tree-width is an important property in computer science: the
complexity of a dynamic programming depends on the tree-width of the problem
dependencies, and CIRCUIT-SAT complexity is linear for a circuit of fixed
tree-width. Tree-width is NP-Hard to compute but is easy to approximate using
heuristics methods.

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{figures/Tree_decomposition.pdf}
    \caption{Tree decomposition of a graph with tree-width 2. A dynamic programming task that depends only on local interactions/edges can traverse the tree decomposition to choose sub-graphs for partial computations. The tree-width is a measure of how many nodes are shared between sub-graphs.\label{fig:treewidth}}
\end{figure}

\subsection{D-Wave machine}

The quantum annealer that is assumed to be used during this thesis is produced
by D-Wave Systems. The basic unit of their machine is the Superconducting
QUantum Interference Device (SQUID). The SQUID consists in a electrical circuit
containing Josephson junctions, a circuit component that exhibits known quantum
effects when exposed to a transversal magnetic field. At low enough
temperatures, the current in the circuit flows in a superposition of clockwise
and counterclockwise direction. These two states form a qubit. Figure
\ref{fig:squid-qubit} shows a simple schema of the device. The user sets the
desired Ising Hamiltonian by controlling the external magnetic field. To
perform an anneal run, the Hamiltonian is slowly turned in a adiabatic fashion
from a standard initial value to the desired desired energy landscape.


\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{figures/old/dwave_junction.png}
    \caption{Illustration of a SQUID qubit. The user sets the various biases $h_i$ and couplings $J_{ij}$ by tuning the various magnetic fields $\Phi$.\label{fig:squid-qubit}. Courtesy of D-Wave Systems Inc.}
\end{figure}


The latest annealer is called D-Wave 2000Q, announced in 2017. Since October
2018 is available to the public through a cloud service\footnote{available at
\url{https://cloud.dwavesys.com/leap/}}. The D-Wave 2000Q machine has 2048
qubits and 5600 couplers in a regular pattern called \textbf{Chimera topology}.
This topology consists in groups of 8 tightly connected qubits in a bipartite
graph called cell, and a grid of cells where 4 qubits have vertical parallel
connections and 4 qubits have horizontal parallel connections. Figure
\ref{fig:chimera-full} shows the induced graph of a D-Wave 2000Q Chimera chip.
\usetikzlibrary{spy}

\begin{figure}[h]
    \centering
    \begin{tikzpicture}[spy using outlines={circle, fill=white}]
        \node[fill=white] at (0,0) {\includegraphics[width=\textwidth]{figures/old/full16.png}};

        \spy[blue, magnification=5, size=5cm] on (-4.15,-4.1) in node at (1,1);
    \end{tikzpicture}
    
    \caption{$16\times16$ Chimera topology with a  detail on a single tile, circled in blue.\label{fig:chimera-full}}
\end{figure}

The Chimera topology is a bipartite graph, thus there are no cliques. This
topology contains as a minor complete graphs and complete bipartite graphs, we will see
then how to encode complete graphs into chimera topology. Thanks to the cell
separation encoded problems often have a clear cut distinction between
functional units, where complex relationship are expressed using the dense
connection within the chip and couplings between cells are used to transfer
information.

%\subsection{the novel Pegasus architecture} 

While all the published work so far has been on the Chimera topology, in 2018
D-Wave divulged details about a newer topology, the so called \textbf{Pegasus
topology}. This topology is more complex and more densely connected, paving the
way for more efficient encodings. Figure \ref{fig:pegasus} illustrates a small
example of a Pegasus 4, containing the equivalent of a square of 4 by 4 tiles.
In this newer topology there is no more a clear decomposition into tiles,
rather than that clusters of qubit are connected in a more interleaved fashion.
Furthermore, the Pegasus architecture adds a new kind of connection between two
horizontal or vertical qubits. This allows the new architecture to have 3 and 4
cliques, that were absent from Chimera.

\begin{figure}[h]
    \centering
    %\usetikzlibrary{spy}
    \begin{tikzpicture}[spy using outlines={circle, fill=white}]
        \node[fill=white] at (0,0) {\includegraphics[width=\textwidth]{figures/pegasus4_gray.pdf}};

        \spy[blue, magnification=3, size=6cm] on (-3.8,-2) in node at (2,2);
    \end{tikzpicture}
    
    \caption{Small pegasus topology, with focus on a single 4-clique. circled in blue.\label{fig:pegasus}}
\end{figure}
